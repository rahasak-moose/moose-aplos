package com.score.aplos.elastic

import java.util

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.HttpMethods.{GET, PUT}
import akka.http.scaladsl.model.{ContentTypes, HttpRequest, StatusCodes}
import akka.stream.ActorMaterializer
import akka.util.ByteString
import com.score.aplos.cassandra._
import com.score.aplos.config.{CassandraConf, ElasticConf}
import com.score.aplos.protocol.{IdentityReply, QualificationReply}
import com.score.aplos.util.{AppLogger, DateFactory}
import org.apache.lucene.search.join.ScoreMode
import org.elasticsearch.common.unit.DistanceUnit
import org.elasticsearch.index.query.QueryBuilders
import org.elasticsearch.search.sort.SortOrder

import scala.collection.JavaConverters._
import scala.concurrent.Await
import scala.concurrent.duration._

object ElasticStore extends ElasticCluster with AppLogger with CassandraConf with ElasticConf {

  case class Sort(field: String, ascending: Boolean)

  case class Criteria(name: String, value: String, mustNot: Boolean = false, should: Boolean = false)

  case class Nested(path: String, criterias: List[Criteria], mustNot: Boolean = false)

  case class GeoFilter(name: String, distance: String, lat: Double, lon: Double)

  case class Range(name: String, from: String, to: String)

  case class Page(offset: Int, limit: Int)

  def init()(implicit system: ActorSystem): Unit = {
    System.setProperty("es.set.netty.runtime.available.processors", "false")

    // create offers index with custom mapping for location(geo_point)
    val disc = "^((?!location).*)"
    val props =
      """
        |"location": {
        |  "type": "geo_point",
        |  "cql_collection": "singleton"
        |}
      """.stripMargin

    initIndex(transElasticIndex, transElasticDocType)
    initIndex(identityElasticIndex, identityElasticDocType)
    initIndex(qualificationElasticIndex, qualificationElasticDocType, Some(disc), Some(props))
  }

  def initIndex(index: String, docTyp: String, disc: Option[String] = None, props: Option[String] = None)(implicit system: ActorSystem): Unit = {
    implicit val ec = system.dispatcher
    implicit val materializer = ActorMaterializer()
    implicit val timeout = 40.seconds

    // params
    val uri = s"http://${elasticHosts.head}:9200/$index"
    val json =
      s"""
      {
        "settings":{
          "keyspace": "$cassandraKeyspace"
        },
        "mappings": {
          "$docTyp" : {
            "discover" : "${disc.getOrElse(".*")}",
            "properties": {
              ${props.getOrElse("")}
            }
          }
        }
      }
    """

    // check index exists
    val get = HttpRequest(GET, uri = uri)
    Await.result(Http().singleRequest(get), timeout).status match {
      case StatusCodes.NotFound =>
        // index not exists
        logger.info(s"init index request uri $uri json $json")

        // create index
        val put = HttpRequest(PUT, uri = uri).withEntity(ContentTypes.`application/json`, ByteString(json.stripLineEnd))
        val resp = Await.result(Http().singleRequest(put), timeout)

        logger.info(s"init index response: $resp")
      case _ =>
        // index already exists
        logger.info(s"$index index already exists")
    }
  }

  def getIdentities(terms: List[Criteria], wildcards: List[Criteria], mustFilter: List[Criteria], shouldFilter: List[Criteria],
                    nested: Option[Nested], geoFilter: Option[GeoFilter], sorts: List[Sort], page: Option[Page]): (Long, List[IdentityReply]) = {
    def toList(obj: Object): List[String] = {
      obj match {
        case strings: java.util.ArrayList[String] =>
          // multiple elements
          strings.asScala.toList
        case string: String =>
          // single elements
          List(string)
        case _ =>
          List()
      }
    }

    def toLatLon(obj: Object) = {
      obj match {
        case obj: java.util.Map[String, AnyRef] =>
          // single elements
          Option(obj.get("lat").asInstanceOf[Double], obj.get("lon").asInstanceOf[Double])
        case _ =>
          None
      }
    }

    // extract offers from search response
    val search = buildSearch(identityElasticIndex, identityElasticDocType, terms, wildcards, mustFilter, shouldFilter, nested, geoFilter, sorts, page)
    val resp = search.execute().actionGet()
    val hits = resp.getHits.getHits
    val accounts = hits.filter(h => h.getSourceAsMap != null)
      .map { hit =>
        val h = hit.getSourceAsMap
        IdentityReply(
          h.get("did").asInstanceOf[String],
          h.get("owner").asInstanceOf[String],
          toList(h.get("roles")),
          toList(h.get("groups")),
          dehydrate(h.get("pub_keu").asInstanceOf[String]),
          dehydrate(h.get("nic").asInstanceOf[String]),
          dehydrate(h.get("name").asInstanceOf[String]),
          dehydrate(h.get("dob").asInstanceOf[String]),
          dehydrate(h.get("phone").asInstanceOf[String]),
          dehydrate(h.get("email").asInstanceOf[String]),
          dehydrate(h.get("province").asInstanceOf[String]),
          dehydrate(h.get("district").asInstanceOf[String]),
          dehydrate(h.get("address").asInstanceOf[String]),
          dehydrate(h.get("blob_id").asInstanceOf[String]),
          None,
          h.get("activated").asInstanceOf[Boolean],
          h.get("disabled").asInstanceOf[Boolean],
          dehydrate(h.get("timestamp").asInstanceOf[String])
        )
      }.toList
    (resp.getHits.totalHits, accounts)
  }

  def getQualifications(terms: List[Criteria], wildcards: List[Criteria], mustFilter: List[Criteria], shouldFilter: List[Criteria],
                        nested: Option[Nested], geoFilter: Option[GeoFilter], sorts: List[Sort], page: Option[Page]): (Long, List[QualificationReply]) = {
    def toLatLon(obj: Object) = {
      obj match {
        case obj: java.util.Map[String, AnyRef] =>
          // single elements
          Option(obj.get("lat").asInstanceOf[Double], obj.get("lon").asInstanceOf[Double])
        case _ =>
          None
      }
    }

    // extract offers from search response
    val search = buildSearch(qualificationElasticIndex, qualificationElasticDocType, terms, wildcards, mustFilter, shouldFilter, nested, geoFilter, sorts, page)
    val resp = search.execute().actionGet()
    val hits = resp.getHits.getHits
    val consents = hits.filter(h => h.getSourceAsMap != null)
      .map { hit =>
        val h = hit.getSourceAsMap
        val alResult = AlResult(
          dehydrate(h.get("al_stream").asInstanceOf[String]),
          dehydrate(h.get("al_subject1").asInstanceOf[String]),
          dehydrate(h.get("al_subject2").asInstanceOf[String]),
          dehydrate(h.get("al_subject3").asInstanceOf[String])
        )
        val olResult = OlResult(
          dehydrate(h.get("ol_mathematics").asInstanceOf[String]),
          dehydrate(h.get("ol_english").asInstanceOf[String]),
          dehydrate(h.get("ol_sinhala").asInstanceOf[String]),
          dehydrate(h.get("ol_tamil").asInstanceOf[String]),
          dehydrate(h.get("ol_science").asInstanceOf[String]),
          dehydrate(h.get("ol_health_science").asInstanceOf[String]),
          dehydrate(h.get("ol_religion").asInstanceOf[String]),
          dehydrate(h.get("ol_art").asInstanceOf[String]),
          dehydrate(h.get("ol_history").asInstanceOf[String]),
          dehydrate(h.get("ol_geography").asInstanceOf[String])
        )
        val eq = EducationalQualification(
          dehydrateB(h.get("has_diploma").asInstanceOf[Boolean]),
          dehydrate(h.get("name_diploma").asInstanceOf[String])
        )
        val pq = ProfessionalQualification(
          dehydrateB(h.get("has_experience").asInstanceOf[Boolean]),
          dehydrate(h.get("name_experience").asInstanceOf[String]),
          dehydrate(h.get("duration_experience").asInstanceOf[String]),
          dehydrate(h.get("profession_area").asInstanceOf[String]),
          dehydrate(h.get("profession_name").asInstanceOf[String])
        )
        val d = DateFactory.formatToDate(h.get("timestamp").asInstanceOf[String], DateFactory.TIMESTAMP_FORMAT_E, DateFactory.UTC_TIME_ZONE)
        val t = DateFactory.formatToString(Option(d), DateFactory.TIMESTAMP_FORMAT, DateFactory.COLOMBO_TIME_ZONE)

        QualificationReply(
          h.get("id").asInstanceOf[String],
          h.get("holder").asInstanceOf[String],
          Option(olResult),
          Option(alResult),
          Option(eq),
          Option(pq),
          dehydrate(h.get("status").asInstanceOf[String]),
          t
        )
      }.toList
    (resp.getHits.totalHits, consents)
  }

  def buildSearch(index: String, docType: String, terms: List[Criteria], wildcards: List[Criteria],
                  mustFilters: List[Criteria], shouldFilters: List[Criteria], nested: Option[Nested],
                  geoFilter: Option[GeoFilter], sorts: List[Sort], page: Option[Page]) = {
    // term query to search on arrays
    val termBuilder = QueryBuilders.boolQuery()
    terms.filter(_.value.nonEmpty).foreach { f =>
      termBuilder.must(QueryBuilders.termQuery(f.name, f.value))
    }

    // wildcard
    val wildcardBuilder = QueryBuilders.boolQuery()
    wildcards.filter(_.value.nonEmpty).foreach { f =>
      wildcardBuilder.should(QueryBuilders.wildcardQuery(f.name, f.value))
    }

    // must filter
    val mustBuilder = QueryBuilders.boolQuery()
    mustFilters.filter(_.value.nonEmpty).foreach { f =>
      if (f.mustNot)
        mustBuilder.mustNot(QueryBuilders.matchQuery(f.name, f.value))
      else
        mustBuilder.must(QueryBuilders.matchQuery(f.name, f.value))
    }

    // should filter
    val shouldBuilder = QueryBuilders.boolQuery()
    shouldFilters.filter(_.value.nonEmpty).foreach { f =>
      shouldBuilder.should(QueryBuilders.matchQuery(f.name, f.value))
    }

    // combine builders with nested
    val builder = QueryBuilders.boolQuery()
    nested match {
      case Some(n) =>
        val nestedFilters = QueryBuilders.boolQuery()
        n.criterias.foreach { f =>
          nestedFilters.must(QueryBuilders.matchQuery(f.name, f.value))
        }

        // nested builder
        val nestedBuilder = QueryBuilders.boolQuery()
        if (n.mustNot)
          nestedBuilder.mustNot(QueryBuilders.nestedQuery(n.path, nestedFilters, ScoreMode.None))
        else
          nestedBuilder.must(QueryBuilders.nestedQuery(n.path, nestedFilters, ScoreMode.None))

        // combine all builders
        builder.must(termBuilder).must(nestedBuilder).must(mustBuilder).must(shouldBuilder).must(wildcardBuilder)
      case None =>
        // combine all builders
        builder.must(termBuilder).must(mustBuilder).must(shouldBuilder).must(wildcardBuilder)
    }

    // geo distance filter
    geoFilter match {
      case Some(gf) =>
        val filter = QueryBuilders.geoDistanceQuery(gf.name)
          .point(gf.lat, gf.lon)
          .distance(gf.distance, DistanceUnit.KILOMETERS)
        builder.must(filter)
      case _ =>
    }

    // build search
    val searchBuilder = client
      .prepareSearch(index)
      .setTypes(docType)
      .setQuery(builder)

    // add sorts
    sorts.foreach { s =>
      searchBuilder.addSort(s.field, if (s.ascending) SortOrder.ASC else SortOrder.DESC)
    }

    // build pagination
    val p = page.getOrElse(Page(0, 10))
    searchBuilder.setFrom(p.offset).setSize(p.limit)
  }

  def dehydrate(value: String): Option[String] = {
    if (value == null) None else Option(value)
  }

  def dehydrateB(value: Boolean): Option[Boolean] = {
    if (value == null) None else Option(value)
  }

}

//object M extends App {
//  import com.score.aplos.elastic.ElasticStore.Criteria
//
//  // wildcards
//  var wildcards = List[Criteria]()
//  wildcards = wildcards :+ Criteria("id", "1212")
//  wildcards = wildcards :+ Criteria("name", "1212")
//
//  // filters with pending state
//  val must = List(Criteria("status", "APPROVED"))
//  val should = List(Criteria("from_user", "2222", should = true), Criteria("to_user", "2222", should = true))
//
//  val resp = ElasticStore.getQualifications(List(), List(), List(), List(), None, None, List(), None)
//  println(resp)
//}
