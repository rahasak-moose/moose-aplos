// To execute Scala code, please define an object named Solution that extends App

import Gen.{Empty, Value}

import scala.annotation.tailrec

trait Gen[T] {
  def pull: Gen.State[T]
}

object Gen {

  sealed trait State[T]

  final case class Value[T](continue: Gen[T], value: T) extends State[T]

  final case class Empty[T]() extends State[T]

}


object Solution extends App {

  def collect(gen: Gen[Int]): List[Int] = {
    @tailrec
    def ops(l: List[Int], gen: Gen[Int]): List[Int] = {
      gen.pull match {
        case Value(continue, value) =>
          ops(l ++ List(value), continue)
        case Empty() =>
          l
      }
    }

    ops(List(), gen)
  }

  case class Clct(gen: Gen[Int]) extends Gen[Int] {
    override def pull: Gen.State[Int] = {
      gen.pull match {
        case Value(continue, value) => Value(Clct(continue), value)
        case Empty() => Empty()
      }
    }
  }

  case class Emits(l: List[Int]) extends Gen[Int] {
    def pull: Gen.State[Int] = {
      l match {
        case x :: xs => Value(Emits(xs), x)
        case _ => Empty()
      }
    }
  }

  case class Map(parent: Gen[Int], f: Int => Int) extends Gen[Int] {
    override def pull: Gen.State[Int] = {
      parent.pull match {
        case Value(continue, value) => Value(Map(continue, f), f(value))
        case Empty() => Empty()
      }
    }
  }

  case class Filter(parent: Gen[Int], predicate: Int => Boolean) extends Gen[Int] {
    override def pull: Gen.State[Int] = {
      parent.pull match {
        case Value(continue, value) => Value(Filter(continue, predicate), value)
        case Empty() => Empty()
      }
    }
  }


  val list: List[Int] = List(1, 2, 3, 4)
  val el = Emits(list)
  val cl = Clct(el)

  println(el)
  println(cl)

  val p = cl.pull
  println(p)


  println(collect(Map(Emits(list), x => x * 2)))
  //println(el)
  //println(doubleList)
  ////assert list == sameList

}