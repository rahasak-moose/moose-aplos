package com.score.aplos.config

import com.typesafe.config.ConfigFactory

import scala.util.Try

trait RedisConf {
  val redisConf = ConfigFactory.load("redis.conf")

  // redis conf
  lazy val redisHost = Try(redisConf.getString("redis.host")).getOrElse("dev.localhost")
  lazy val redisPort = Try(redisConf.getInt("redis.port")).getOrElse(6379)
  lazy val redisKey = Try(redisConf.getString("redis.key")).getOrElse("rahasak")
}
