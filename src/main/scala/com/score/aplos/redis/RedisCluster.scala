package com.score.aplos.redis

import com.score.aplos.config.RedisConf
import redis.clients.jedis.{JedisPool, JedisPoolConfig}

trait RedisCluster extends RedisConf {

  lazy val poolConfig = {
    val pool = new JedisPoolConfig()
    pool.setMaxTotal(128)
    pool.setBlockWhenExhausted(true)
    pool
  }
  lazy val redisPool = new JedisPool(poolConfig, redisHost, redisPort)

}
