package com.score.aplos.util

import java.io.{File, FileInputStream, FileOutputStream}
import java.security.spec.{PKCS8EncodedKeySpec, RSAPublicKeySpec, X509EncodedKeySpec}
import java.security.{KeyFactory, _}

import com.score.aplos.config.AppConf
import javax.crypto.Cipher
import org.bouncycastle.asn1.{ASN1Integer, ASN1Sequence}
import sun.misc.{BASE64Decoder, BASE64Encoder}

import scala.util.{Failure, Success, Try}

object CryptoFactory extends AppConf with AppLogger {

  def init(): Unit = {
    CryptoFactory.initRSAKeys()
    logger.info(s"init Crypto factory, pubkey - ${CryptoFactory.loadRSAPublicKey()}")
  }

  def initRSAKeys(): Unit = {
    // first create .keys directory
    val dir: File = new File(keysDir)
    if (!dir.exists) {
      dir.mkdir
    }

    // generate keys if not exists
    val filePublicKey = new File(publicKeyLocation)
    if (!filePublicKey.exists) {
      generateRSAKeyPair()
    }
  }

  def generateRSAKeyPair(): Unit = {
    // generate key pair
    val keyPairGenerator = KeyPairGenerator.getInstance("RSA")
    keyPairGenerator.initialize(keysSize, new SecureRandom)
    val keyPair: KeyPair = keyPairGenerator.generateKeyPair

    // save public key
    val x509keySpec = new X509EncodedKeySpec(keyPair.getPublic.getEncoded)
    val publicKeyStream = new FileOutputStream(publicKeyLocation)
    publicKeyStream.write(x509keySpec.getEncoded)

    // save private key
    val pkcs8KeySpec = new PKCS8EncodedKeySpec(keyPair.getPrivate.getEncoded)
    val privateKeyStream = new FileOutputStream(privateKeyLocation)
    privateKeyStream.write(pkcs8KeySpec.getEncoded)
  }

  def loadRSAKeyPair(): KeyPair = {
    // read public key
    val filePublicKey = new File(publicKeyLocation)
    var inputStream = new FileInputStream(publicKeyLocation)
    val encodedPublicKey: Array[Byte] = new Array[Byte](filePublicKey.length.toInt)
    inputStream.read(encodedPublicKey)
    inputStream.close()

    // read private key
    val filePrivateKey = new File(privateKeyLocation)
    inputStream = new FileInputStream(privateKeyLocation)
    val encodedPrivateKey: Array[Byte] = new Array[Byte](filePrivateKey.length.toInt)
    inputStream.read(encodedPrivateKey)
    inputStream.close()

    val keyFactory: KeyFactory = KeyFactory.getInstance("RSA")

    // public key
    val publicKeySpec: X509EncodedKeySpec = new X509EncodedKeySpec(encodedPublicKey)
    val publicKey: PublicKey = keyFactory.generatePublic(publicKeySpec)

    // private key
    val privateKeySpec: PKCS8EncodedKeySpec = new PKCS8EncodedKeySpec(encodedPrivateKey)
    val privateKey: PrivateKey = keyFactory.generatePrivate(privateKeySpec)

    new KeyPair(publicKey, privateKey)
  }

  def loadRSAPublicKey(): String = {
    // get public key via key pair
    val keyPair = loadRSAKeyPair()
    val publicKeyStream = keyPair.getPublic.getEncoded

    // BASE64 encoded string
    new BASE64Encoder().encode(publicKeyStream).replaceAll("\n", "").replaceAll("\r", "")
  }

  def getRSAPublicKey(pubkey: String): Option[PublicKey] = {
    def getAppleKey: Try[PublicKey] = {
      Try {
        // these will throw exception in case of type mismatch
        val sequence = ASN1Sequence.getInstance(new BASE64Decoder().decodeBuffer(pubkey))
        val modulus = ASN1Integer.getInstance(sequence.getObjectAt(0))
        val exponent = ASN1Integer.getInstance(sequence.getObjectAt(1))
        val keySpec = new RSAPublicKeySpec(modulus.getPositiveValue, exponent.getPositiveValue)
        val factory = KeyFactory.getInstance("RSA")
        factory.generatePublic(keySpec)
      }
    }

    def getAndroidKey: Try[PublicKey] = {
      Try {
        val spec = new X509EncodedKeySpec(new BASE64Decoder().decodeBuffer(pubkey))
        val kf = KeyFactory.getInstance("RSA")
        kf.generatePublic(spec)
      }
    }

    getAndroidKey match {
      case Success(pk) => Option(pk)
      case Failure(_) => getAppleKey.toOption
    }
  }

  def sign(payload: String) = {
    // get private key via key pair
    val keyPair = loadRSAKeyPair()
    val privateKey = keyPair.getPrivate

    // sign the payload
    val signature: Signature = Signature.getInstance("SHA256withRSA")
    signature.initSign(privateKey)
    signature.update(payload.getBytes)

    // signature as Base64 encoded string
    new BASE64Encoder().encode(signature.sign).replaceAll("\n", "").replaceAll("\r", "")
  }

  def verifySignature(payload: String, signedPayload: String): Boolean = {
    // get public key via key pair
    val keyPair = loadRSAKeyPair()
    val publicKey = keyPair.getPublic

    val signature = Signature.getInstance("SHA256withRSA")
    signature.initVerify(publicKey)
    signature.update(payload.getBytes)

    // decode(BASE64) signed payload and verify signature
    signature.verify(new BASE64Decoder().decodeBuffer(signedPayload))
  }

  def verifySignature(payload: String, signedPayload: String, publicKey: Option[PublicKey]) = {
    publicKey.exists { pk =>
      val signature = Signature.getInstance("SHA256withRSA")
      signature.initVerify(pk)
      signature.update(payload.getBytes)

      // decode(BASE64) signed payload and verify signature
      signature.verify(new BASE64Decoder().decodeBuffer(signedPayload))
    }
  }

  def encrypt(payload: String, publicKey: PublicKey): Array[Byte] = {
    val cipher: Cipher = Cipher.getInstance("RSA")
    cipher.init(Cipher.ENCRYPT_MODE, publicKey)

    cipher.doFinal(payload.getBytes)
  }

  def decrypt(payload: Array[Byte], privateKey: PrivateKey): String = {
    val cipher: Cipher = Cipher.getInstance("RSA")
    cipher.init(Cipher.DECRYPT_MODE, privateKey)
    new String(cipher.doFinal(payload))
  }

  def sha256(payload: String): String = {
    val digest = MessageDigest.getInstance("SHA-256")
    val hash = digest.digest(payload.getBytes)

    new BASE64Encoder().encode(hash).replaceAll("\n", "").replaceAll("\r", "")
  }

  def encode(paylod: String): String = {
    // signature as Base64 encoded string
    new BASE64Encoder().encode(paylod.getBytes())
      .replaceAll("\n", "")
      .replaceAll("\r", "")
  }

}

//object M extends App {
//
//  import spray.json._
//
//  case class Ops(id: String, name: String, age: String)
//
//  import com.score.aplos.protocol.PromizeMessageProtocol._
//
//  val a = Approve("approve", "eranga", "1111", "2222", "hak", "ops", "640", "")
//
//  val j = a.toJson.asJsObject
//  val x = j.fields.values.toSeq.map(_.toString().replaceAll("\"", "")).sortWith(_ < _).mkString
//  //println(j)
//  //println(x)
//  //println(x == "11112222640approveerangahakops")
//
//  // 11112222640approveerangahakops
//
//  CryptoFactory.initRSAKeys()
//  //println(CryptoFactory.loadRSAPublicKey())
//
//  val puk = "MIGJAoGBAKvuOT/7mmqAfhHDd3nEzrXuHbcieRl8TC34CVNfoWETNSg7Mfyv1uw8o+xOFXkJdcNlcyQiP/JXlPXp0TTpPw69rntBCEzd0AU/ztKu5R9vN82skEuwG5FQPW18ACQ6aH9NpdcU2b54ZChQl2XhXkC0QekXNLdUhNi+Yob+8tq9AgMBAAE="
//  val px = CryptoFactory.getRSAPublicKey(puk)
//
//  val digz = "YdMgCHLJC0hrL8EwHwKSBMVmg80oGWKQLzjRA9ttFUcW766IYdhnFWiABuj/VhrLD8US4GWwy5oERxMR06dZ3EGW1Uf0YSqF2htrWhQDdmOSQhlQASyvq2jk4EftIfKRMmEuuq4fHBbAJhw1iK/186No2wZ5x2o6Ra3Zd2NhRE8="
//
//  val pubKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCeeHTadazwPG4rnZuhXpPcXIrBFK+P68r7otQeRCn1ptdg4a9IgX9JYoVnu6iR7X8Ai6O1LPHb9IIjxX1qSzrCI0PT45w1uJR2YR8L2jKDjqFpKoEsg/7Svvmr5rQNMW++rRnuJ9ysOjTEngjvJh1oOwOQg0SrxYUTSh4KSBjM0wIDAQAB"
//  val pk = CryptoFactory.getRSAPublicKey(pubKey)
//
//  val sig = CryptoFactory.sign("rahasak")
//  println(sig)
//
//  val v = CryptoFactory.verifySignature("rahasak", digz, px)
//  println(v)
//
//  //val sig1 = CryptoFactory.sign("100873cccccreatexxxxyyyyyy")
//  //println(sig1)
//  //val v1 = CryptoFactory.verifySignature("100873cccccreatexxxxyyyyyy", sig1, pk)
//  //println(v1)
//
//  // QfSQG9ONaksY7NDWOFaIT51NlQfSJfFQXyu3c4zE1QkmsXRnxFaiGr1tt6XL1qXYS3d86zAVC5AEBtSgo9I6AINevsn/8Pi746gXPPAAJQRbJMlYOqepTVII1K45oZCROWQHxU8rE/IVKBcv2mO+mS9YOKSvGJVXp4GXVCGpXdM=
//  // R7vXUEwiDq2ZMRquwuRgo80Jk4cjhXOa2jlCb2iD2oZo4k9h6JnbKHFwRKPcDU5avARCJlDQgkCmsE35UDXhS/W1jHXbUDg1wpJ5W5NMC4wIwlyr7tDXCRd4dYsZhyKogG3gqA3wU2bKHmFB6SO1Zab9N9MFWzzE948PIqtHMvc=
//}

//object M extends App {
//  val j =
//    """
//      |{
//      |    "employee": {
//      |        "name":       "sonoo",
//      |        "salary":      56000,
//      |        "married":    true,
//      |        "digsig":    haha,
//      |        "ado": ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
//      |    }
//      |}
//    """.stripMargin
//
//  val a =
//    """
//      |{  "geodata": [    {      "id": "1",      "name": "Julie Sherman",      "gender" : "female",      "latitude" : "37.33774833333334",      "longitude" : "-121.88670166666667"    },    {      "id": "2",      "name": "Johnny Depp",      "gender" : "male",      "latitude" : "37.336453",      "longitude" : "-121.884985"    }  ]}
//    """.stripMargin
//
//    val digsig = "haha"
//    val x = a.replaceAll("[\\[\\](){}]", "")
//      .replaceAll(" ", "")
//      .replaceAll("\"", "")
//      .replaceAll(",", "")
//      .replaceAll(":", "")
//      .replaceAll("\n", "")
//      .replaceAll("\r", "")
//      .replaceAll(digsig, "")
//      .toLowerCase()
//      .sortWith(_ < _)
//    println(x)
//}

