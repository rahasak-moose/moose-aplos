package com.score.aplos.actor

import java.net.URLEncoder
import java.util.{Date, Random}

import akka.actor.{Actor, Props}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.ActorMaterializer
import com.score.aplos.actor.IdentityActor._
import com.score.aplos.cassandra._
import com.score.aplos.config.{ApiConf, AppConf, FeatureToggleConf, SmsConf}
import com.score.aplos.elastic.ElasticStore
import com.score.aplos.elastic.ElasticStore.{Criteria, Page, Sort}
import com.score.aplos.protocol._
import com.score.aplos.redis.RedisStore
import com.score.aplos.util.{AppLogger, CryptoFactory, DateFactory}
import spray.json.DefaultJsonProtocol._
import spray.json._

import scala.concurrent.Await
import scala.concurrent.duration._
import scala.util.Try

object IdentityActor {

  case class Create(messageType: String, execer: String, id: String,
                    did: String, owner: String,
                    password: String, roles: List[String], groups: List[String],
                    pubkey: Option[String],
                    nic: Option[String],
                    name: Option[String],
                    gender: Option[String],
                    age: Option[Int],
                    dob: Option[String],
                    phone: Option[String],
                    email: Option[String],
                    address: Option[String],
                    province: Option[String],
                    district: Option[String],
                    lat: Option[Double],
                    lon: Option[Double],
                    blob: Option[String]) extends IdentityMessage

  case class Register(messageType: String, execer: String, id: String, did: String, owner: String, password: String,
                      pubkey: Option[String], deviceToken: Option[String], deviceType: Option[String], deviceImei: Option[String]) extends IdentityMessage

  case class Activate(messageType: String, execer: String, id: String, did: String, owner: String, salt: String) extends IdentityMessage

  case class Deactivate(messageType: String, execer: String, id: String, did: String, owner: String) extends IdentityMessage

  case class Connect(messageType: String, execer: String, id: String, did: String, owner: String, password: String) extends IdentityMessage

  case class ChangePassword(messageType: String, execer: String, id: String, did: String, owner: String, oldPassword: String, newPassword: String) extends IdentityMessage

  case class ResetPassword(messageType: String, execer: String, id: String, did: String, owner: String, nic: String, phone: String, email: String, answer1: String, answer2: String, answer3: String) extends IdentityMessage

  case class ResetPasswordConfirm(messageType: String, execer: String, id: String, did: String, owner: String, newPassword: String, salt: String) extends IdentityMessage

  case class UpdateDevice(messageType: String, execer: String, id: String, did: String, owner: String,
                          deviceType: Option[String], deviceToken: Option[String], deviceImie: Option[String]) extends IdentityMessage

  case class UpdateAnswers(messageType: String, execer: String, id: String, did: String, owner: String,
                           answer1: Option[String], answer2: Option[String], answer3: Option[String]) extends IdentityMessage

  case class UpdateRoles(messageType: String, execer: String, id: String, did: String, owner: String, roles: List[String]) extends IdentityMessage

  case class Get(messageType: String, execer: String, id: String, did: String, owner: String) extends IdentityMessage

  case class Search(messageType: String, execer: String, id: String, offset: String, limit: String, didTerm: String, nameTerm: String,
                    ownerTerm: String, did: String, owner: String, typ: String, nic: String, phone: String, email: String, name: String, sort:
                    String = "descending") extends IdentityMessage

  def props()(implicit materializer: ActorMaterializer) = Props(new IdentityActor)

}

class IdentityActor()(implicit materializer: ActorMaterializer) extends Actor with AppConf with ApiConf with SmsConf with FeatureToggleConf with AppLogger {
  override def receive: Receive = {
    case create: Create =>
      logger.info(s"got create message - $create")

      // verify digital signature
      if (enableVerifySignature) {
        CryptoFactory.verifySignature("digsig", create.toString)
      }

      CassandraStore.getIdentity(create.did, create.owner) match {
        case Some(acc) =>
          // already existing verified account
          logger.error(s"activated account already exists - $acc")
          sender ! StatusReply(400, "activated account already exists with given did.")
        case _ =>
          // no activated account exists
          // get execer user
          // execer comes with <user>:<owner>
          val arr = create.execer.split(":")
          CassandraStore.getIdentity(arr(0).trim, arr(1).trim) match {
            case Some(eAcc) =>
              // create user on behalf of another user, e.g bank create it's agents, central bank create companies
              if (eAcc.roles.contains("company") || eAcc.roles.contains("admin")) {
                // have permission to create account
                // create trans
                implicit val format: JsonFormat[Create] = jsonFormat22(Create)
                val trans = Trans(create.id, create.execer, "IdentityActor", "Create", create.toJson.toString, create.pubkey.getOrElse(""), "digsig")
                CassandraStore.createTrans(trans)

                // create not active account
                val salt = (100000 + new Random().nextInt(900000)).toString

                // create account
                val acc = Identity(
                  create.did, create.owner,
                  create.password, create.roles.toSet, create.groups.toSet, create.pubkey,
                  None, None, None,
                  create.name, create.nic, create.gender, create.age, None, create.phone, create.email,
                  None, None, None,
                  create.address, create.province, create.district, None, None, None,
                  None, None, None,
                  salt, 0, activated = false, disabled = false)
                CassandraStore.createIdentity(acc)

                logger.info(s"account created - $create")
                sender ! StatusReply(201, "Created")
              } else {
                // no permission to create account
                // Unauthorized
                sender ! StatusReply(401, "Unauthorized request.")
              }
            case None =>
              // create normal user, e.g create identity from connect mobile app
              // owner should be there
              CassandraStore.getIdentity(create.owner, create.owner) match {
                case Some(_) =>
                  // owner is there
                  // create trans
                  implicit val format: JsonFormat[Create] = jsonFormat22(Create)
                  val trans = Trans(create.id, create.execer, "IdentityActor", "Create", create.toJson.toString, create.pubkey.getOrElse(""), "")
                  CassandraStore.createTrans(trans)

                  // create blob
                  val blobId = create.blob.map { b =>
                    CassandraStore.createBlob(Blob(create.did, b))
                    create.did
                  }

                  // create account
                  val dob = create.dob.flatMap(d => DateFactory.formatToLocalDate(d, DateFactory.DATE_FORMAT))
                  val salt = (100000 + new Random().nextInt(900000)).toString
                  val location = for {
                    lat <- create.lat
                    lon <- create.lon
                  } yield Location(lat, lon)

                  val acc = Identity(
                    create.did, create.owner,
                    create.password, create.roles.toSet, create.groups.toSet, create.pubkey,
                    None, None, None,
                    create.name, create.nic, create.gender, create.age, dob, create.phone, create.email,
                    None, None, blobId,
                    create.address, create.province, create.district, None, None, location,
                    None, None, None,
                    salt, 0, activated = true, disabled = false)
                  CassandraStore.createIdentity(acc)

                  // token reply
                  implicit val tokenFormat: JsonFormat[Token] = jsonFormat6(Token.apply)
                  val token = Token(acc.did, acc.roles.mkString(","), acc.groups.mkString(","), DateFactory.timestamp(), 60)
                  sender ! TokenReply(201, CryptoFactory.encode(token.toJson.toString))
                case None =>
                  // owner not there
                  logger.error(s"invalid account owner - $create")
                  sender ! StatusReply(400, "Invalid account owner.")
              }
          }
      }

      context.stop(self)

    case register: Register =>
      logger.info(s"got account register message - $register")

      // verify digital signature
      if (enableVerifySignature) {
        CryptoFactory.verifySignature("digsig", register.toString)
      }

      // first check double spend
      val id = s"${register.execer};IdentityActor;${register.id}"
      if (!CassandraStore.isDoubleSpend(register.execer, "IdentityActor", register.id) && RedisStore.set(id)) {
        CassandraStore.getIdentity(register.did, register.owner) match {
          case Some(acc) =>
            if (acc.activated) {
              // already registered account exists
              logger.error(s"activated account already exists - $register")
              sender ! StatusReply(400, "Activated account exists with id")
            } else {
              // found not activated account
              // create trans
              implicit val format: JsonFormat[Register] = jsonFormat10(Register)
              val trans = Trans(register.id, register.execer, "IdentityActor", "Register", register.toJson.toString, acc.pubkey.getOrElse(""), "")
              CassandraStore.createTrans(trans)

              // update password and other fields
              val s = "93121"
              val uAcc = Identity(
                register.did, register.owner,
                register.password, Set(), Set(), register.pubkey,
                salt = s, attempts = 0, activated = true, disabled = false)
              CassandraStore.registerIdentity(uAcc)

              // token reply
              implicit val tokenFormat: JsonFormat[Token] = jsonFormat6(Token.apply)
              val token = Token(acc.did, acc.roles.mkString(","), acc.groups.mkString(","), DateFactory.timestamp(), 60)
              sender ! TokenReply(201, CryptoFactory.encode(token.toJson.toString))
            }
          case None =>
            // should have created account to register from mobile
            logger.error(s"account does not exists to register - $register")
            sender ! StatusReply(400, "No account found with user id")
        }
      } else {
        logger.error(s"double spend register - $register")
        sender ! StatusReply(400, "Double spend")
      }

      context.stop(self)

    case activate: Activate =>
      logger.info(s"got activate message - $activate")

      // verify digital signature
      if (enableVerifySignature) {
        CryptoFactory.verifySignature("digsig", activate.toString)
      }

      // first check double spend
      val id = s"${activate.execer};IdentityActor;${activate.id}"
      if (!CassandraStore.isDoubleSpend(activate.execer, "IdentityActor", activate.id) && RedisStore.set(id)) {
        CassandraStore.getIdentity(activate.did, activate.owner) match {
          case Some(acc) =>
            // check account activate attempts
            if (acc.attempts > 3) {
              // no existing account
              logger.error(s"exceed account activate attempts - $acc")
              sender ! StatusReply(400, "Failed to activate account, exceed activate attempts.")
            } else {
              // compare salt
              if (acc.salt.equals(activate.salt)) {
                // valid salt
                // create trans
                implicit val format: JsonFormat[Activate] = jsonFormat6(Activate)
                val trans = Trans(activate.id, activate.execer, "IdentityActor", "Activate", activate.toJson.toString, acc.pubkey.getOrElse(""), "")
                CassandraStore.createTrans(trans)

                // update attempts
                // activate account
                CassandraStore.updateAttempts(acc.did, acc.owner, acc.attempts + 1)
                CassandraStore.activateIdentity(acc.did, acc.owner, activated = true)

                // token reply
                implicit val tokenFormat: JsonFormat[Token] = jsonFormat6(Token.apply)
                val token = Token(acc.did, acc.roles.mkString(","), acc.groups.mkString(","), DateFactory.timestamp(), 60)
                sender ! TokenReply(200, CryptoFactory.encode(token.toJson.toString))
              } else {
                // no existing account
                logger.error(s"invalid salt to activate account - $acc")
                CassandraStore.updateAttempts(acc.did, acc.owner, acc.attempts + 1)

                sender ! StatusReply(400, "Verification code is incorrect. Please enter correct Verification code.")
              }
            }
          case None =>
            // no existing account
            logger.error(s"no account found for - ${activate.did}, ${activate.owner}")

            sender ! StatusReply(400, "Failed to activate account, your NIC is not registered in Promize.")
        }
      } else {
        logger.error(s"double spend activate $activate")

        sender ! StatusReply(400, "double spend")
      }

      context.stop(self)

    case deactivate: Deactivate =>
      logger.info(s"got deactivate message - $deactivate")

      // verify digital signature
      if (enableVerifySignature) {
        CryptoFactory.verifySignature("digsig", deactivate.toString)
      }

      val arr = deactivate.execer.split(":")
      CassandraStore.getIdentity(arr(0).trim, arr(1).trim) match {
        case Some(eAcc) =>
          if (eAcc.did.equalsIgnoreCase(deactivate.owner)) {
            // valid owner
            CassandraStore.activateIdentity(deactivate.did, deactivate.owner, activated = false)
            sender ! StatusReply(200, "Updated")
          } else {
            sender ! StatusReply(400, "invalid account owner")
          }
        case _ =>
          sender ! StatusReply(400, "invalid request")
      }

      context.stop(self)

    case changePassword: ChangePassword =>
      logger.info(s"got changePassword message - $changePassword")

      // verify digital signature
      if (enableVerifySignature) {
        CryptoFactory.verifySignature("digsig", changePassword.toString)
      }

      // first check double spend
      val id = s"${changePassword.execer};IdentityActor;${changePassword.id}"
      if (!CassandraStore.isDoubleSpend(changePassword.execer, "IdentityActor", changePassword.id) && RedisStore.set(id)) {
        CassandraStore.getIdentity(changePassword.did, changePassword.owner) match {
          case Some(acc) =>
            // check account activate attempts
            if (acc.activated) {
              if (acc.password.equals(changePassword.oldPassword)) {
                CassandraStore.updatePassword(changePassword.did, changePassword.owner, changePassword.newPassword)

                // token reply
                implicit val tokenFormat: JsonFormat[Token] = jsonFormat6(Token.apply)
                val token = Token(acc.did, acc.roles.mkString(","), acc.groups.mkString(","), DateFactory.timestamp(), 60)
                sender ! TokenReply(200, CryptoFactory.encode(token.toJson.toString))
              } else {
                logger.error(s"invalid password - $acc")
                sender ! StatusReply(401, "Failed to change password, current password is incorrect.")
              }
            } else {
              // not activated acc
              logger.error(s"account not activated - $acc")
              sender ! StatusReply(400, "Failed to change password, your account is not activated.")
            }
          case None =>
            // no existing account
            logger.error(s"no account found for - ${changePassword.did}")

            sender ! StatusReply(401, "Failed to change the password, your NIC is not registered in Promize.")
        }
      } else {
        logger.error(s"double spend activate $changePassword")

        sender ! StatusReply(400, "Double spend")
      }

      context.stop(self)

    case reset: ResetPassword =>
      logger.info(s"got ResetPassword message - $reset")

      // verify digital signature
      if (enableVerifySignature) {
        CryptoFactory.verifySignature("digsig", reset.toString)
      }

      // first check double spend
      val id = s"${reset.execer};IdentityActor;${reset.id}"
      if (!CassandraStore.isDoubleSpend(reset.execer, "IdentityActor", reset.id) && RedisStore.set(id)) {
        CassandraStore.getIdentity(reset.did, reset.owner) match {
          case Some(acc) =>
            var i = 0
            for {
              a1 <- acc.answer1
              a2 <- acc.answer2
              a3 <- acc.answer3
            } {
              if (a1 == reset.answer1) i = i + 1
              if (a2 == reset.answer2) i = i + 1
              if (a3 == reset.answer3) i = i + 1
              i
            }

            if (i >= 2) {
              // answered two questions
              // post SMS to customer mobile with salt
              val salt = (100000 + new Random().nextInt(900000)).toString
              val msg = s"Connect app verification code for reset password - $salt"

              for {
                p <- acc.phone
              } yield {
                sendSms(Sms(p, msg))
              } match {
                case 200 =>
                  logger.info(s"success send password reset verification code sms, $reset")

                  // create trans
                  implicit val format: JsonFormat[ResetPassword] = jsonFormat11(ResetPassword)
                  val trans = Trans(reset.id, reset.execer, "IdentityActor", "ResetPassword", reset.toJson.toString, acc.pubkey.getOrElse(""), "")
                  CassandraStore.createTrans(trans)

                  // make account deactivate
                  CassandraStore.activateIdentity(acc.did, acc.owner, activated = false)
                  CassandraStore.updateSalt(acc.did, acc.owner, salt)
                  CassandraStore.updateAttempts(acc.did, acc.owner, 0)

                  // send token reply
                  implicit val tokenFormat: JsonFormat[Token] = jsonFormat6(Token.apply)
                  val token = Token(acc.did, acc.roles.mkString(","), acc.groups.mkString(","), DateFactory.timestamp(), 60)
                  sender ! TokenReply(200, CryptoFactory.encode(token.toJson.toString))
                case s =>
                  logger.info(s"fail send verification code sms - $s")
                  sender ! StatusReply(400, "Failed to reset password, error while sending password reset verification code via SMS.")
              }
            } else {
              logger.error(s"security question answers are incorrect - $reset")
              sender ! StatusReply(400, "Security question answers are incorrect. You have to give correct answers for two security questions.")
            }
          case None =>
            // no existing account
            logger.error(s"no account found for - ${reset.did}")

            sender ! StatusReply(400, "Account details are incorrect. Please make sure your NIC, Account number and Phone number are correct.")
        }
      } else {
        logger.error(s"double spend activate $reset")

        sender ! StatusReply(400, "Double spend")
      }

      context.stop(self)

    case reset: ResetPasswordConfirm =>
      logger.info(s"got ResetPasswordConfirm message - $reset")

      // verify digital signature
      if (enableVerifySignature) {
        CryptoFactory.verifySignature("digsig", reset.toString)
      }

      // first check double spend
      val id = s"${reset.execer};IdentityActor;${reset.id}"
      if (!CassandraStore.isDoubleSpend(reset.execer, "IdentityActor", reset.id) && RedisStore.set(id)) {
        CassandraStore.getIdentity(reset.did, reset.owner) match {
          case Some(acc) =>
            if (!acc.activated) {
              if (acc.attempts > 3) {
                // no existing account
                logger.error(s"exceed account activate attempts - $acc")
                sender ! StatusReply(400, "Failed to activate account, exceed activate attempts")
              } else {
                // compare salt
                if (acc.salt.equals(reset.salt)) {
                  // valid salt
                  // create trans
                  implicit val format: JsonFormat[ResetPasswordConfirm] = jsonFormat7(ResetPasswordConfirm)
                  val trans = Trans(reset.id, reset.execer, "IdentityActor", "ResetPasswordConfirm", reset.toJson.toString, acc.pubkey.getOrElse(""), "")
                  CassandraStore.createTrans(trans)

                  // update attempts
                  // activate account
                  // update password
                  CassandraStore.updateAttempts(acc.did, reset.owner, 0)
                  CassandraStore.activateIdentity(acc.did, reset.owner, activated = true)
                  CassandraStore.updatePassword(acc.did, reset.owner, reset.newPassword)

                  // send status
                  sender ! StatusReply(200, "Updated")
                } else {
                  // invalid salt
                  logger.error(s"invalid salt to activate account - $acc")
                  CassandraStore.updateAttempts(acc.did, reset.owner, acc.attempts + 1)

                  sender ! StatusReply(400, "Verification code is incorrect. Please enter correct verification code.")
                }
              }
            } else {
              // not activated acc
              logger.error(s"account already activated - $acc")
              sender ! StatusReply(400, "Failed to reset password, account already activated.")
            }
          case None =>
            // no existing account
            logger.error(s"no account found for - ${reset.did}, ${reset.owner}")

            sender ! StatusReply(400, "Failed to reset the password, your NIC is not registered in Promize.")
        }
      } else {
        logger.error(s"double spend activate $reset")

        sender ! StatusReply(400, "Double spend")
      }

      context.stop(self)

    case updateDevice: UpdateDevice =>
      logger.info(s"got update device message - $updateDevice")

      // verify digital signature
      if (enableVerifySignature) {
        CryptoFactory.verifySignature("digsig", updateDevice.toString)
      }

      // first check double spend
      val id = s"${updateDevice.execer};IdentityActor;${updateDevice.id}"
      if (!CassandraStore.isDoubleSpend(updateDevice.execer, "IdentityActor", updateDevice.id) && RedisStore.set(id)) {
        CassandraStore.getIdentity(updateDevice.did, updateDevice.owner) match {
          case Some(acc) =>
            // create trans
            implicit val activateFormat: JsonFormat[UpdateDevice] = jsonFormat8(UpdateDevice)
            val trans = Trans(updateDevice.id, updateDevice.execer, "IdentityActor", "UpdateDevice", updateDevice.toJson.toString, acc.pubkey.getOrElse(""), "")
            CassandraStore.createTrans(trans)

            // update device
            val device = Device(updateDevice.deviceToken, updateDevice.deviceType, updateDevice.deviceImie)
            CassandraStore.updateDevice(acc.did, acc.owner, device)
            sender ! StatusReply(200, "Updated")
          case None =>
            // no existing account
            logger.error(s"no account found for to update - ${updateDevice.did}, ${updateDevice.owner}")

            sender ! StatusReply(400, "Failed to update device, your NIC is not registered in Promize.")
        }
      } else {
        logger.error(s"double spend update $updateDevice")

        sender ! StatusReply(400, "double spend")
      }

      context.stop(self)

    case updateAnswers: UpdateAnswers =>
      logger.info(s"got update answers message - $updateAnswers")

      // verify digital signature
      if (enableVerifySignature) {
        CryptoFactory.verifySignature("digsig", updateAnswers.toString)
      }

      // first check double spend
      val id = s"${updateAnswers.execer};IdentityActor;${updateAnswers.id}"
      if (!CassandraStore.isDoubleSpend(updateAnswers.execer, "IdentityActor", updateAnswers.id) && RedisStore.set(id)) {
        CassandraStore.getIdentity(updateAnswers.did, updateAnswers.owner) match {
          case Some(acc) =>
            // create trans
            implicit val formal: JsonFormat[UpdateAnswers] = jsonFormat8(UpdateAnswers)
            val trans = Trans(updateAnswers.id, updateAnswers.execer, "IdentityActor", "UpdateAnswers", updateAnswers.toJson.toString, acc.pubkey.getOrElse(""), "")
            CassandraStore.createTrans(trans)

            // update device
            val answer = Answer(updateAnswers.answer1, updateAnswers.answer2, updateAnswers.answer3)
            CassandraStore.updateAnswer(acc.did, acc.owner, answer)
            sender ! StatusReply(200, "Updated")
          case None =>
            // no existing account
            logger.error(s"no account found for to update - ${updateAnswers.did}, ${updateAnswers.owner}")

            sender ! StatusReply(400, "Failed to update answers, your NIC is not registered in Promize.")
        }
      } else {
        logger.error(s"double spend update $updateAnswers")

        sender ! StatusReply(400, "double spend")
      }

      context.stop(self)

    case connect: Connect =>
      logger.info(s"got connect account message - $connect")

      // verify digital signature
      if (enableVerifySignature) {
        CryptoFactory.verifySignature("digsig", connect.toString)
      }

      // first check double spend
      val id = s"${connect.execer};IdentityActor;${connect.id}"
      if (!CassandraStore.isDoubleSpend(connect.execer, "IdentityActor", connect.id) && RedisStore.set(id)) {
        CassandraStore.getIdentity(connect.did, connect.owner) match {
          case Some(acc) =>
            // check account activate attempts
            if (acc.activated) {
              if (acc.password.equals(connect.password)) {
                // check qualification exists for execer
                if (CassandraStore.getQualifications(connect.did).isEmpty) {
                  // no qualification
                  // token reply with mention need to update qualification
                  implicit val tokenFormat: JsonFormat[Token] = jsonFormat6(Token.apply)
                  val token = Token(acc.did, acc.roles.mkString(","), acc.groups.mkString(","), DateFactory.timestamp(), 60)
                  sender ! TokenReply(405, CryptoFactory.encode(token.toJson.toString))
                } else {
                  // all done, token reply with login done
                  implicit val tokenFormat: JsonFormat[Token] = jsonFormat6(Token.apply)
                  val token = Token(acc.did, acc.roles.mkString(","), acc.groups.mkString(","), DateFactory.timestamp(), 60)
                  sender ! TokenReply(200, CryptoFactory.encode(token.toJson.toString))
                }
              } else {
                logger.error(s"invalid password - $acc")
                CassandraStore.updateAttempts(acc.did, acc.owner, acc.attempts + 1)

                sender ! StatusReply(401, "Password or User ID is Incorrect.")
              }
            } else {
              // not activated acc
              logger.error(s"account not activated - $acc")

              // token reply
              implicit val tokenFormat: JsonFormat[Token] = jsonFormat6(Token.apply)
              val token = Token(acc.did, acc.roles.mkString(","), acc.groups.mkString(","), DateFactory.timestamp(), 60)
              sender ! TokenReply(402, CryptoFactory.encode(token.toJson.toString))
            }
          case None =>
            // no existing account
            logger.error(s"no account found for - ${connect.did}, ${connect.owner}")

            sender ! StatusReply(401, "Password or User ID is incorrect.")
        }
      } else {
        logger.error(s"double spend activate $connect")

        sender ! StatusReply(400, "Double spend")
      }

      context.stop(self)

    case get: Get =>
      logger.info(s"got get message - $get")

      // verify digital signature
      if (enableVerifySignature) {
        CryptoFactory.verifySignature("digsig", get.toString)
      }

      CassandraStore.getIdentity(get.did, get.owner) match {
        case Some(a) =>
          val blob = for {
            id <- a.blobId
            b <- CassandraStore.getBlob(id)
          } yield b.blob
          val dob = DateFactory.formatToString(a.dob.map(d => new Date(d.getMillisSinceEpoch)), DateFactory.DATE_FORMAT, DateFactory.COLOMBO_TIME_ZONE)

          sender ! IdentityReply(a.did, a.owner, a.roles.toList, a.groups.toList, a.pubkey, a.nic, a.name, dob, a.phone, a.email,
            a.province, a.district, a.address, a.blobId, Option(blob.getOrElse("")), a.activated, a.disabled,
            DateFactory.formatToString(Option(a.timestamp), DateFactory.TIMESTAMP_FORMAT, DateFactory.COLOMBO_TIME_ZONE))
        case _ =>
          logger.error(s"no matching account found - ${get.did}, ${get.owner}")
          sender ! StatusReply(404, "Failed to get account, account not found")
      }

      context.stop(self)

    case search: Search =>
      logger.info(s"got search message - $search")

      // verify digital signature
      if (enableVerifySignature) {
        CryptoFactory.verifySignature("digsig", search.toString)
      }

      // check execer account and roles
      val arr = search.execer.split(":")
      CassandraStore.getIdentity(arr(0).trim, arr(1).trim) match {
        case Some(acc) =>
          // wildcards
          var wildcards = List[Criteria]()
          if (!search.didTerm.isEmpty) wildcards = wildcards :+ Criteria("did", search.didTerm)
          if (!search.nameTerm.isEmpty) wildcards = wildcards :+ Criteria("name", search.nameTerm)
          if (!search.ownerTerm.isEmpty) wildcards = wildcards :+ Criteria("owner", search.nameTerm)

          // filters
          var criterias = List[Criteria]()
          if (acc.roles.contains("admin")) {
            // get all accounts for admin user
            criterias = criterias :+ Criteria("nic", search.nic)
            criterias = criterias :+ Criteria("phone", search.phone)
            criterias = criterias :+ Criteria("typ", search.typ)
          } else {
            // get accounts own by user
            criterias = criterias :+ Criteria("did", search.did)
            criterias = criterias :+ Criteria("owner", search.owner)
            criterias = criterias :+ Criteria("nic", search.nic)
            criterias = criterias :+ Criteria("phone", search.phone)
            criterias = criterias :+ Criteria("typ", search.typ)
          }

          // sorts and pages
          val sorts = List(Sort("timestamp", if (search.sort.equalsIgnoreCase("ascending")) true else false))
          val page = Page(Try(search.offset.toInt).getOrElse(0), Try(search.limit.toInt).getOrElse(10))

          val (hits, accounts) = ElasticStore.getIdentities(List(), wildcards, criterias, List(), None, None, sorts, Option(page))
          val meta = IdentityMeta(search.offset, search.limit, accounts.size.toString, hits.toString)
          val searchReply = IdentitySearchReply(meta, accounts)
          sender ! searchReply
        case None =>
          logger.error(s"not registered execer - ${search.execer}")
          sender ! StatusReply(403, "Unauthorized execer")
      }

      context.stop(self)
  }

  def sendSms(sms: Sms): Int = {
    logger.info(s"post sms $sms, api $smsApi, use-send-sms $useSendSms")

    val msg = URLEncoder.encode(sms.msg, "UTF-8").replace("+", "%20")
    val uri = s"$smsApi?q=$smsApiKey&destination=${sms.phone}&message=$msg"

    if (useSendSms) {
      implicit val ex = context.dispatcher
      val future = Http(context.system).singleRequest(
        HttpRequest(
          HttpMethods.GET,
          uri
        )
      ).flatMap { response =>
        Unmarshal(response.entity).to[String]
      }.map { responseStr =>
        logger.info(s"got response $responseStr")
        responseStr match {
          case "0" =>
            200
          case _ =>
            400
        }
      }.recover {
        case e =>
          logError(e)
          400
      }
      Await.result(future, 60.seconds)
    } else {
      200
    }
  }
}
