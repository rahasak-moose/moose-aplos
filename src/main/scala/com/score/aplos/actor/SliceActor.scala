package com.score.aplos.actor

import akka.actor.{Actor, Props}
import akka.stream.ActorMaterializer
import com.score.aplos.actor.SliceActor.{AddSlice, DeleteSlice, UpdateSlice}
import com.score.aplos.cassandra._
import com.score.aplos.config.{ApiConf, AppConf, FeatureToggleConf}
import com.score.aplos.protocol._
import com.score.aplos.redis.RedisStore
import com.score.aplos.util.{AppLogger, CryptoFactory, SliceFactory}
import spray.json.DefaultJsonProtocol._
import spray.json._


object SliceActor {

  case class AddSlice(messageType: String, execer: String, id: String, digsig: String,
                      sliceId: String, dataSpeed: Double, capacity: Double, quality: String, latency: Double, sla: String) extends SliceMessage

  case class UpdateSlice(messageType: String, execer: String, id: String, digsig: String,
                         sliceId: String, dataSpeed: Double, capacity: Double, quality: String, latency: Double, sla: String) extends SliceMessage

  case class DeleteSlice(messageType: String, execer: String, id: String, digsig: String,
                         sliceId: String) extends SliceMessage

  def props()(implicit materializer: ActorMaterializer) = Props(new SliceActor())

}

class SliceActor()(implicit materializer: ActorMaterializer) extends Actor with AppConf with ApiConf with FeatureToggleConf with AppLogger {

  override def receive: Receive = {
    case msg: AddSlice =>
      logger.info(s"got AddSlice message - $msg")

      // verify digital signature
      if (enableVerifySignature) {
        CryptoFactory.verifySignature(msg.digsig, msg.toString)
      }

      val id = s"${msg.execer};AddSlice;${msg.id}"
      if (!CassandraStore.isDoubleSpend(msg.execer, "ScliceActor", msg.id) && RedisStore.set(id)) {
        implicit val format: JsonFormat[AddSlice] = jsonFormat10(AddSlice)
        val trans = Trans(msg.id, msg.execer, "SliceActor", "AddSlice", msg.toJson.toString, "pubkey", msg.digsig)
        CassandraStore.createTrans(trans)

        val slice = Slice(msg.sliceId, msg.dataSpeed, msg.capacity, msg.quality, msg.latency, msg.sla)

        // match slice requirements based on request params
        val template = SliceFactory.matchSliceTemplate(slice)
        SliceFactory.createSlice(slice, template)

        sender ! StatusReply(200, "slice created")
      } else {
        logger.error(s"double spend AddSlice $msg")

        sender ! StatusReply(400, "double spend")
      }

      context.stop(self)

    case msg: UpdateSlice =>
      logger.info(s"got UpdateSlice message - $msg")

      // verify digital signature
      if (enableVerifySignature) {
        CryptoFactory.verifySignature(msg.digsig, msg.toString)
      }

      val id = s"${msg.execer};UpdateSlice;${msg.id}"
      if (!CassandraStore.isDoubleSpend(msg.execer, "ScliceActor", msg.id) && RedisStore.set(id)) {
        implicit val format: JsonFormat[UpdateSlice] = jsonFormat10(UpdateSlice)
        val trans = Trans(msg.id, msg.execer, "SliceActor", "UpdateSlice", msg.toJson.toString, "pubkey", msg.digsig)
        CassandraStore.createTrans(trans)

        val slice = Slice(msg.sliceId, msg.dataSpeed, msg.capacity, msg.quality, msg.latency, msg.sla)

        // match slice requirements based on request params
        // update slice via katana
        val template = SliceFactory.matchSliceTemplate(slice)
        SliceFactory.createSlice(slice, template)

        sender ! StatusReply(200, "slice updated")
      } else {
        logger.error(s"double spend UpdateSlice $msg")

        sender ! StatusReply(400, "double spend")
      }

      context.stop(self)

    case msg: DeleteSlice =>
      logger.info(s"got DeleteSlice message - $msg")

      // verify digital signature
      if (enableVerifySignature) {
        CryptoFactory.verifySignature(msg.digsig, msg.toString)
      }

      val id = s"${msg.execer};DeleteSlice;${msg.id}"
      if (!CassandraStore.isDoubleSpend(msg.execer, "ScliceActor", msg.id) && RedisStore.set(id)) {
        implicit val format: JsonFormat[DeleteSlice] = jsonFormat5(DeleteSlice)
        val trans = Trans(msg.id, msg.execer, "SliceActor", "DeleteSlice", msg.toJson.toString, "pubkey", msg.digsig)
        CassandraStore.createTrans(trans)

        // invoke katana api to update slice

        sender ! StatusReply(200, "slice deleted")
      } else {
        logger.error(s"double spend DeleteSlice $msg")

        sender ! StatusReply(400, "double spend")
      }

      context.stop(self)
  }

}
