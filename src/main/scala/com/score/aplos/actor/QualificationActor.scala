package com.score.aplos.actor

import akka.actor.{Actor, Props}
import akka.stream.ActorMaterializer
import com.score.aplos.actor.QualificationActor._
import com.score.aplos.cassandra._
import com.score.aplos.config.{ApiConf, AppConf, FeatureToggleConf}
import com.score.aplos.elastic.ElasticStore
import com.score.aplos.elastic.ElasticStore.{Criteria, Page, Sort}
import com.score.aplos.protocol._
import com.score.aplos.redis.RedisStore
import com.score.aplos.util.{AppLogger, CryptoFactory, DateFactory}
import spray.json._

import scala.util.Try

object QualificationActor {

  case class CreateQualification(messageType: String, execer: String, id: String, digsig: String,
                                 qualificationId: String, qualificationHolder: String,
                                 olResult: Option[OlResult],
                                 alResult: Option[AlResult],
                                 educationalQualification: Option[EducationalQualification],
                                 professionalQualification: Option[ProfessionalQualification]) extends QualificationMessage

  case class AddQualification(messageType: String, execer: String, id: String, digsig: String,
                              qualificationId: String, qualificationHolder: String,

                              hasDiploma: Option[Boolean] = None,
                              nameDiploma: Option[String],

                              hasExperience: Option[Boolean] = None,
                              nameExperience: Option[String],
                              durationExperience: Option[String],

                              professionArea: Option[String],
                              professionName: Option[String]) extends QualificationMessage

  case class AddEducationalQualification(messageType: String, execer: String, id: String, digsig: String,
                                         qualificationId: String, qualificationHolder: String,

                                         hasDiploma: Option[Boolean] = None,
                                         nameDiploma: Option[String]) extends QualificationMessage

  case class AddProfessionalQualification(messageType: String, execer: String, id: String, digsig: String,
                                          qualificationId: String, qualificationHolder: String,
                                          hasExperience: Option[Boolean] = None,
                                          nameExperience: Option[String],
                                          durationExperience: Option[String],
                                          professionArea: Option[String],
                                          professionName: Option[String]) extends QualificationMessage

  case class AddOlResult(messageType: String, execer: String, id: String, digsig: String,
                         qualificationId: String, qualificationHolder: String,
                         mathematics: Option[String],
                         english: Option[String],
                         sinhala: Option[String],
                         tamil: Option[String],
                         science: Option[String],
                         healthScience: Option[String],
                         religion: Option[String],
                         art: Option[String],
                         history: Option[String],
                         geography: Option[String]) extends QualificationMessage

  case class AddAlResult(messageType: String, execer: String, id: String, digsig: String,
                         qualificationId: String, qualificationHolder: String,
                         stream: Option[String],
                         subject1: Option[String],
                         subject2: Option[String],
                         subject3: Option[String]) extends QualificationMessage


  case class UpdateQualificationStatus(messageType: String, execer: String, id: String, digsig: String,
                                       qualificationId: String, qualificationHolder: String,
                                       status: String) extends QualificationMessage

  case class GetQualification(messageType: String, execer: String, id: String, digsig: String,
                              qualificationId: String, qualificationHolder: String) extends QualificationMessage

  case class GetQualifications(messageType: String, execer: String, id: String, digsig: String,
                               qualificationHolder: String) extends QualificationMessage

  case class SearchQualification(messageType: String, execer: String, id: String, digsig: String,
                                 offset: String,
                                 limit: String,
                                 holder: String,
                                 holderTerm: String,
                                 profession: String,
                                 professionTerm: String,
                                 status: String,
                                 sort: String = "descending") extends QualificationMessage

  case class UpdateQualification(messageType: String, execer: String, id: String, digsig: String,
                                 qualificationId: String, qualificationHolder: String,
                                 olResult: Option[OlResult],
                                 alResult: Option[AlResult],
                                 educationalQualification: Option[EducationalQualification],
                                 professionalQualification: Option[ProfessionalQualification]) extends QualificationMessage

  def props()(implicit materializer: ActorMaterializer) = Props(new QualificationActor())

}

class QualificationActor()(implicit materializer: ActorMaterializer) extends Actor with AppConf with ApiConf with FeatureToggleConf with AppLogger {

  override def receive: Receive = {
    case add: AddQualification =>
      logger.info(s"got AddQualification message - $add")

      // verify and process message
      if (verifyMessage(add)) {
        val qualification = Qualification(
          add.qualificationId,
          add.qualificationHolder,

          hasDiploma = add.hasDiploma,
          nameDiploma = add.nameDiploma,
          hasExperience = add.hasExperience,
          nameExperience = add.nameExperience,
          durationExperience = add.durationExperience,

          professionArea = add.professionArea,
          professionName = add.professionName
        )
        CassandraStore.createQualification(qualification)

        sender ! StatusReply(201, "Created")
      } else {
        sender ! StatusReply(400, "invalid message")
      }

      context.stop(self)

    case create: CreateQualification =>
      logger.info(s"got CreateQualification message - $create")

      // verify and process message
      if (verifyMessage(create)) {

        val qualification = Qualification(
          create.qualificationId,
          create.qualificationHolder,
          create.olResult.flatMap(_.olMathematics),
          create.olResult.flatMap(_.olEnglish),
          create.olResult.flatMap(_.olSinhala),
          create.olResult.flatMap(_.olTamil),
          create.olResult.flatMap(_.olScience),
          create.olResult.flatMap(_.olHealthScience),
          create.olResult.flatMap(_.olReligion),
          create.olResult.flatMap(_.olArt),
          create.olResult.flatMap(_.olHistory),
          create.olResult.flatMap(_.olGeography),
          create.alResult.flatMap(_.stream),
          create.alResult.flatMap(_.subject1),
          create.alResult.flatMap(_.subject2),
          create.alResult.flatMap(_.subject3),
          create.educationalQualification.flatMap(_.hasDiploma),
          create.educationalQualification.flatMap(_.nameDiploma),
          create.professionalQualification.flatMap(_.hasExperience),
          create.professionalQualification.flatMap(_.nameExperience),
          create.professionalQualification.flatMap(_.durationExperience),
          create.professionalQualification.flatMap(_.professionArea),
          create.professionalQualification.flatMap(_.professionName),
          Option("pending"))
        CassandraStore.createQualification(qualification)

        sender ! StatusReply(201, "Created")
      } else {
        sender ! StatusReply(400, "invalid message")
      }

      context.stop(self)

    case update: AddEducationalQualification =>
      logger.info(s"got AddEducationalQualification message - $update")

      // verify and process message
      if (verifyMessage(update)) {
        val qualification = EducationalQualification(
          update.hasDiploma,
          update.nameDiploma
        )
        CassandraStore.addEducationalQualification(update.id, update.qualificationHolder, qualification)

        sender ! StatusReply(200, "OK")
      } else {
        sender ! StatusReply(400, "invalid message")
      }

      context.stop(self)

    case update: AddProfessionalQualification =>
      logger.info(s"got AddProfessionalQualification message - $update")

      // verify and process message
      if (verifyMessage(update)) {
        val qualification = ProfessionalQualification(
          update.hasExperience,
          update.nameExperience,
          update.durationExperience,
          update.professionArea,
          update.professionName
        )
        CassandraStore.addProfessionalQualification(update.id, update.qualificationHolder, qualification)

        sender ! StatusReply(200, "OK")
      } else {
        sender ! StatusReply(400, "invalid message")
      }

      context.stop(self)

    case update: AddOlResult =>
      logger.info(s"got AddOlResult message - $update")

      // verify and process message
      if (verifyMessage(update)) {
        val result = OlResult(
          update.mathematics,
          update.english,
          update.sinhala,
          update.tamil,
          update.science,
          update.healthScience,
          update.religion,
          update.art,
          update.history,
          update.geography
        )
        CassandraStore.addOlResult(update.id, update.qualificationHolder, result)

        sender ! StatusReply(200, "OK")
      } else {
        sender ! StatusReply(400, "invalid message")
      }

      context.stop(self)

    case update: AddAlResult =>
      logger.info(s"got AddAlResult message - $update")

      // verify and process message
      if (verifyMessage(update)) {
        val result = AlResult(
          update.stream,
          update.subject1,
          update.subject2,
          update.subject3
        )
        CassandraStore.addAlResult(update.id, update.qualificationHolder, result)

        sender ! StatusReply(200, "OK")
      } else {
        sender ! StatusReply(400, "invalid message")
      }

      context.stop(self)

    case update: UpdateQualificationStatus =>
      logger.info(s"got UpdateQualificationStatus message - $update")

      // verify digital signature
      if (enableVerifySignature) {
        CryptoFactory.verifySignature(update.digsig, update.toString)
      }

      if (verifyMessage(update)) {
        CassandraStore.updateQualificationStatus(update.qualificationId, update.qualificationHolder, Option(update.status))

        sender ! StatusReply(200, "OK")
      } else {
        sender ! StatusReply(400, "invalid message")
      }

      context.stop(self)

    case update: UpdateQualification =>
      logger.info(s"got updateQualification message - $update")

      // verify digital signature
      if (verifyMessage(update)) {
        val qualification = Qualification(
          update.qualificationId,
          update.qualificationHolder,
          update.olResult.flatMap(_.olMathematics),
          update.olResult.flatMap(_.olEnglish),
          update.olResult.flatMap(_.olSinhala),
          update.olResult.flatMap(_.olTamil),
          update.olResult.flatMap(_.olScience),
          update.olResult.flatMap(_.olHealthScience),
          update.olResult.flatMap(_.olReligion),
          update.olResult.flatMap(_.olArt),
          update.olResult.flatMap(_.olHistory),
          update.olResult.flatMap(_.olGeography),
          update.alResult.flatMap(_.stream),
          update.alResult.flatMap(_.subject1),
          update.alResult.flatMap(_.subject2),
          update.alResult.flatMap(_.subject3),
          update.educationalQualification.flatMap(_.hasDiploma),
          update.educationalQualification.flatMap(_.nameDiploma),
          update.professionalQualification.flatMap(_.hasExperience),
          update.professionalQualification.flatMap(_.nameExperience),
          update.professionalQualification.flatMap(_.durationExperience),
          update.professionalQualification.flatMap(_.professionArea),
          update.professionalQualification.flatMap(_.professionName))
        CassandraStore.updateQualificationAsync(qualification)

        sender ! StatusReply(200, "Updated")
      } else {
        sender ! StatusReply(400, "invalid message")
      }

      context.stop(self)

    case get: GetQualification =>
      logger.info(s"got GetQualification message - $get")

      // verify digital signature
      if (enableVerifySignature) {
        CryptoFactory.verifySignature(get.digsig, get.toString)
      }

      // get qualifications
      CassandraStore.getQualification(get.qualificationId, get.qualificationHolder) match {
        case Some(q) =>
          val reply = QualificationReply(
            q.id,
            q.holder,
            Option(OlResult(
              q.olMathematics,
              q.olEnglish,
              q.olSinhala,
              q.olTamil,
              q.olScience,
              q.olHealthScience,
              q.olReligion,
              q.olArt,
              q.olHistory,
              q.olGeography
            )),
            None,
            Option(EducationalQualification(
              q.hasDiploma,
              q.nameDiploma
            )),
            Option(ProfessionalQualification(
              q.hasExperience,
              q.nameExperience,
              q.durationExperience,
              q.professionArea,
              q.professionName
            )),
            q.status,
            DateFactory.formatToString(Option(q.timestamp), DateFactory.TIMESTAMP_FORMAT, DateFactory.COLOMBO_TIME_ZONE)
          )
          sender ! reply
        case None =>
          sender ! StatusReply(404, "qualification not found")
      }

      context.stop(self)

    case get: GetQualifications =>
      logger.info(s"got GetQualifications message - $get")

      // verify digital signature
      if (enableVerifySignature) {
        CryptoFactory.verifySignature(get.digsig, get.toString)
      }

      // get qualifications
      val q = CassandraStore.getQualifications(get.qualificationHolder)
      logger.info(s"got qualifications - $q")

      if (q.nonEmpty) {
        val reply = QualificationReply(
          q.head.id,
          q.head.holder,
          Option(OlResult(
            q.head.olMathematics,
            q.head.olEnglish,
            q.head.olSinhala,
            q.head.olTamil,
            q.head.olScience,
            q.head.olHealthScience,
            q.head.olReligion,
            q.head.olArt,
            q.head.olHistory,
            q.head.olGeography
          )),
          None,
          Option(EducationalQualification(
            q.head.hasDiploma,
            q.head.nameDiploma
          )),
          Option(ProfessionalQualification(
            q.head.hasExperience,
            q.head.nameExperience,
            q.head.durationExperience,
            q.head.professionArea,
            q.head.professionName
          )),
          q.head.status,
          DateFactory.formatToString(Option(q.head.timestamp), DateFactory.TIMESTAMP_FORMAT, DateFactory.COLOMBO_TIME_ZONE)
        )
        sender ! reply
      } else {
        sender ! StatusReply(404, "qualifications not found")
      }

      context.stop(self)

    case search: SearchQualification =>
      logger.info(s"got SearchQualification message - $search")

      // verify digital signature
      if (enableVerifySignature) {
        CryptoFactory.verifySignature(search.digsig, search.toString)
      }

      // wildcards
      var wildcards = List[Criteria]()
      if (!search.holderTerm.isEmpty) wildcards = wildcards :+ Criteria("holder", search.holderTerm)
      if (!search.professionTerm.isEmpty) wildcards = wildcards :+ Criteria("profession_name", search.professionTerm)

      // filters
      var criterias = List[Criteria]()
      criterias = criterias :+ Criteria("holder", search.holder)
      criterias = criterias :+ Criteria("profession", search.profession)
      criterias = criterias :+ Criteria("status", search.status)

      // sorts and pages
      val sorts = List(Sort("timestamp", if (search.sort.equalsIgnoreCase("ascending")) true else false))
      val page = Page(Try(search.offset.toInt).getOrElse(0), Try(search.limit.toInt).getOrElse(10))

      val (hits, projects) = ElasticStore.getQualifications(List(), wildcards, criterias, List(), None, None, sorts, Option(page))
      val meta = QualificationMeta(search.offset, search.limit, projects.size.toString, hits.toString)
      val searchReply = QualificationSearchReply(meta, projects)
      sender ! searchReply

      context.stop(self)
  }

  def verifyMessage(promizeMessage: QualificationMessage) = {
    import QualificationMessageProtocol._
    val json = promizeMessage.toJson.asJsObject
    val execer = json.getFields("execer").head.toString.replaceAll("\"", "")
    val id = json.getFields("id").head.toString.replaceAll("\"", "")
    val digsig = json.getFields("digsig").head.toString.replaceAll("\"", "")
    //    val msg = json.fields.values.map(_.toString()
    //      .replaceAll("\"", "")
    //      .replaceAll("\n", "")
    //      .replaceAll("\r", ""))
    //      .toList
    //      .filter(p => p != digsig)
    //      .sortWith(_ < _).mkString

    val msg = json.toString
      .replace(digsig, "")
      .replace("digsig", "")
      .replaceAll("\"", "")
      .replaceAll("[\\[\\](){}]", "")
      .replaceAll(" ", "")
      .replaceAll(",", "")
      .replaceAll(":", "")
      .replaceAll("\n", "")
      .replaceAll("\r", "")
      .toLowerCase()
      .sortWith(_ < _)

    logger.info(s"json message - ${json.toString}")
    logger.info(s"verify signature - $digsig")
    logger.info(s"verify message - $msg")

    // check for execer
    CassandraStore.getIdentity(execer.split(":")(0), execer.split(";")(1)) match {
      case Some(eAcc) =>
        // check digsig
        if (CryptoFactory.verifySignature(msg, digsig, CryptoFactory.getRSAPublicKey(eAcc.pubkey.get))) {
          // check double spend
          val mid = s"$execer;TransferActor;$id"
          if (!CassandraStore.isDoubleSpend(execer, "TransferActor", id) && RedisStore.set(mid)) {
            // forward message to self for further processing
            true
          } else {
            // double spend
            false
          }
        } else {
          false
        }
      case None =>
        // fail verify digital signature
        false
    }
  }

}
