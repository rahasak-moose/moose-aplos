package com.score.aplos.http

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.{Route, StandardRoute}
import akka.pattern.ask
import akka.stream.{ActorMaterializer, ActorMaterializerSettings, Supervision}
import akka.util.Timeout
import com.score.aplos.actor.{IdentityActor, QualificationActor, SliceActor}
import com.score.aplos.config.AppConf
import com.score.aplos.protocol._
import com.score.aplos.util.AppLogger

import scala.concurrent.ExecutionContextExecutor
import scala.concurrent.duration._
import scala.util.{Failure, Success}

object HttpRouter extends AppLogger with AppConf {

  def serve()(implicit system: ActorSystem): Unit = {
    // meterializer for streams
    val decider: Supervision.Decider = { e =>
      logError(e)
      Supervision.Resume
    }

    implicit val ec = system.dispatcher
    implicit val materializer = ActorMaterializer(ActorMaterializerSettings(system).withSupervisionStrategy(decider))
    Http().bindAndHandle(route, "0.0.0.0", servicePort)
  }

  def route()(implicit materializer: ActorMaterializer, ec: ExecutionContextExecutor, system: ActorSystem): Route = {
    implicit val timeout = Timeout(90.seconds)

    import com.score.aplos.protocol.IdentityMessageProtocol._
    import com.score.aplos.protocol.SliceMessageProtocol._
    import com.score.aplos.protocol.StatusReplyProtocol._

    pathPrefix("api") {
      path("slices") {
        get {
          complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "slice contract"))
        } ~
          post {
            entity(as[SliceMessage]) {
              msg: SliceMessage =>
                val f = system.actorOf(SliceActor.props()) ? msg
                onComplete(f)(reply => handleReply(reply))
            }
          }
      } ~ path("identities") {
        get {
          complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "identity contract"))
        } ~
          post {
            entity(as[IdentityMessage]) {
              msg: IdentityMessage =>
                val f = system.actorOf(IdentityActor.props()) ? msg
                onComplete(f)(reply => handleReply(reply))
            }
          }
      }
    }
  }

  def handleReply(msg: Any): StandardRoute = {
    import com.score.aplos.protocol.StatusReplyProtocol._

    msg match {
      case Success(status: StatusReply) =>
        logger.info(s"reply $status")
        complete {
          if (status.code == 200) StatusCodes.OK -> status
          else if (status.code == 201) StatusCodes.Created -> status
          else if (status.code == 404) StatusCodes.NotFound -> status
          else StatusCodes.BadRequest -> status
        }
      case Success(token: TokenReply) =>
        import com.score.aplos.protocol.TokenReplyProtocol._
        logger.info(s"reply $token")
        complete {
          if (token.status == 201) StatusCodes.Created -> token
          else if (token.status == 200 || token.status == 405) StatusCodes.OK -> token
          else StatusCodes.BadRequest -> token
        }
      case Success(reply: IdentityReply) =>
        logger.debug(s"reply $reply")
        import com.score.aplos.protocol.IdentityReplyProtocol._
        complete(StatusCodes.OK -> reply)
      case Success(reply: IdentitySearchReply) =>
        logger.info(s"reply $reply")
        import com.score.aplos.protocol.IdentitySearchReplyProtocol._
        complete(StatusCodes.OK -> reply)
      case Success(reply: QualificationReply) =>
        logger.info(s"reply $reply")
        import com.score.aplos.protocol.QualificationReplyProtocol._
        complete(StatusCodes.OK -> reply)
      case Success(reply: QualificationSearchReply) =>
        logger.info(s"reply $reply")
        import com.score.aplos.protocol.QualificationSearchReplyProtocol._
        complete(StatusCodes.OK -> reply)
      case Failure(e) =>
        logError(e)
        complete(StatusCodes.BadRequest -> StatusReply(400, "Failed to complete the request"))
      case _ =>
        logger.info("error reply")
        complete(StatusCodes.BadRequest -> StatusReply(400, "Failed to complete the request"))
    }
  }

}
