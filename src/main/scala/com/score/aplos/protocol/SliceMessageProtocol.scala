package com.score.aplos.protocol

import com.score.aplos.actor.SliceActor._
import spray.json._

trait SliceMessage

object SliceMessageProtocol extends DefaultJsonProtocol {

  implicit val format1: JsonFormat[AddSlice] = jsonFormat10(AddSlice)
  implicit val format2: JsonFormat[UpdateSlice] = jsonFormat10(UpdateSlice)
  implicit val format3: JsonFormat[DeleteSlice] = jsonFormat5(DeleteSlice)

  implicit object SliceMessageFormat extends RootJsonFormat[SliceMessage] {
    def write(obj: SliceMessage): JsValue =
      JsObject((obj match {
        case p: AddSlice => p.toJson
        case p: UpdateSlice => p.toJson
        case p: DeleteSlice => p.toJson
        case unknown => deserializationError(s"json deserialize error: $unknown")
      }).asJsObject.fields)

    def read(json: JsValue): SliceMessage =
      json.asJsObject.getFields("messageType") match {
        case Seq(JsString("addSlice")) => json.convertTo[AddSlice]
        case Seq(JsString("updateSlice")) => json.convertTo[UpdateSlice]
        case Seq(JsString("deleteSlice")) => json.convertTo[DeleteSlice]
        case unrecognized => serializationError(s"json serialization error $unrecognized")
      }
  }

}
