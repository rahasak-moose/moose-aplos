package com.score.aplos.protocol

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import com.score.aplos.cassandra.{AlResult, EducationalQualification, OlResult, ProfessionalQualification}
import spray.json.DefaultJsonProtocol

case class QualificationReply(id: String,
                              holder: String,

                              olResult: Option[OlResult] = None,
                              alResult: Option[AlResult] = None,
                              educationalQualification: Option[EducationalQualification] = None,
                              professionalQualification: Option[ProfessionalQualification] = None,

                              status: Option[String] = None,
                              timestamp: Option[String])

object QualificationReplyProtocol extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val f1 = jsonFormat10(OlResult)
  implicit val f2 = jsonFormat4(AlResult)
  implicit val f3 = jsonFormat2(EducationalQualification)
  implicit val f4 = jsonFormat5(ProfessionalQualification)
  implicit val f5 = jsonFormat8(QualificationReply)
}
