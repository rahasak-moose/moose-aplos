package com.score.aplos.protocol

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import com.score.aplos.util.CryptoFactory
import spray.json.DefaultJsonProtocol

object Token {
  def apply(id: String, roles: String, groups: String, issueTime: Long, ttl: Long): Token = {
    val digsig = CryptoFactory.sign(s"$id$roles$issueTime$ttl".toLowerCase)
    Token(id, roles, groups, issueTime, ttl, digsig)
  }
}

case class Token(id: String, roles: String, groups: String, issueTime: Long, ttl: Long, digsig: String)

case class TokenReply(status: Int, token: String)

object TokenReplyProtocol extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val format = jsonFormat2(TokenReply)
}
