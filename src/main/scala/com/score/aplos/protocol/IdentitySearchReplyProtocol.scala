package com.score.aplos.protocol

import spray.json.{DefaultJsonProtocol, _}

case class IdentityMeta(offset: String, limit: String, count: String, total: String)

case class IdentitySearchReply(meta: IdentityMeta, accounts: List[IdentityReply])

object IdentitySearchReplyProtocol extends DefaultJsonProtocol {
  implicit val metaFormat: JsonFormat[IdentityMeta] = jsonFormat4(IdentityMeta)
  import com.score.aplos.protocol.IdentityReplyProtocol._

  implicit object IdentitySearchReplyFormat extends RootJsonFormat[IdentitySearchReply] {
    override def write(obj: IdentitySearchReply): JsValue = {
      JsObject(
        ("meta", obj.meta.toJson),
        ("accounts", obj.accounts.toJson)
      )
    }

    override def read(json: JsValue) = ???
  }

}



