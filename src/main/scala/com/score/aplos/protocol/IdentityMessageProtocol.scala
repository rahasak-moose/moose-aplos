package com.score.aplos.protocol

import com.score.aplos.actor.IdentityActor._
import spray.json._

trait IdentityMessage

object IdentityMessageProtocol extends DefaultJsonProtocol {

  implicit val createFormat: JsonFormat[Create] = jsonFormat22(Create)
  implicit val registerFormat: JsonFormat[Register] = jsonFormat10(Register)
  implicit val activateFormat: JsonFormat[Activate] = jsonFormat6(Activate)
  implicit val deactivateFormat: JsonFormat[Deactivate] = jsonFormat5(Deactivate)
  implicit val connectFormat: JsonFormat[Connect] = jsonFormat6(Connect)
  implicit val getFormat: JsonFormat[Get] = jsonFormat5(Get)
  implicit val addRoleFormat: JsonFormat[UpdateRoles] = jsonFormat6(UpdateRoles)
  implicit val searchFormat: JsonFormat[Search] = jsonFormat16(Search)
  implicit val changePasswordFormat: JsonFormat[ChangePassword] = jsonFormat7(ChangePassword)
  implicit val resetPasswordFormat: JsonFormat[ResetPassword] = jsonFormat11(ResetPassword)
  implicit val resetPasswordConfirmFormat: JsonFormat[ResetPasswordConfirm] = jsonFormat7(ResetPasswordConfirm)
  implicit val updateDeviceFormat: JsonFormat[UpdateDevice] = jsonFormat8(UpdateDevice)
  implicit val updateAnswersFormat: JsonFormat[UpdateAnswers] = jsonFormat8(UpdateAnswers)

  implicit object IdentityMessageFormat extends RootJsonFormat[IdentityMessage] {
    def write(obj: IdentityMessage): JsValue =
      JsObject((obj match {
        case c: Create => c.toJson
        case r: Register => r.toJson
        case a: Activate => a.toJson
        case d: Deactivate => d.toJson
        case c: Connect => c.toJson
        case g: Get => g.toJson
        case r: UpdateRoles => r.toJson
        case s: Search => s.toJson
        case cp: ChangePassword => cp.toJson
        case rp: ResetPassword => rp.toJson
        case ud: UpdateDevice => ud.toJson
        case ua: UpdateAnswers => ua.toJson
        case unknown => deserializationError(s"json deserialize error: $unknown")
      }).asJsObject.fields)

    def read(json: JsValue): IdentityMessage =
      json.asJsObject.getFields("messageType") match {
        case Seq(JsString("create")) => json.convertTo[Create]
        case Seq(JsString("register")) => json.convertTo[Register]
        case Seq(JsString("activate")) => json.convertTo[Activate]
        case Seq(JsString("deactivate")) => json.convertTo[Deactivate]
        case Seq(JsString("connect")) => json.convertTo[Connect]
        case Seq(JsString("get")) => json.convertTo[Get]
        case Seq(JsString("addRole")) => json.convertTo[UpdateRoles]
        case Seq(JsString("search")) => json.convertTo[Search]
        case Seq(JsString("changePassword")) => json.convertTo[ChangePassword]
        case Seq(JsString("resetPassword")) => json.convertTo[ResetPassword]
        case Seq(JsString("resetPasswordConfirm")) => json.convertTo[ResetPasswordConfirm]
        case Seq(JsString("updateDevice")) => json.convertTo[UpdateDevice]
        case Seq(JsString("updateAnswers")) => json.convertTo[UpdateAnswers]
        case unrecognized => serializationError(s"json serialization error $unrecognized")
      }
  }

}

//object M extends App {
//
//  import AccountMessageProtocol._
//
//  val i = Get("get", "23232", "23232", "3232")
//  val s = i.toJson.toString
//  println(s)
//  s.parseJson.convertTo[AccountMessage] match {
//    case i: Create => println(s"init $i")
//    case g: Get => println(s"get $g")
//  }
//
//}

