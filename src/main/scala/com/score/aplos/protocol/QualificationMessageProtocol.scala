package com.score.aplos.protocol

import com.score.aplos.actor.QualificationActor._
import com.score.aplos.cassandra.{AlResult, EducationalQualification, OlResult, ProfessionalQualification}
import spray.json._

trait QualificationMessage

object QualificationMessageProtocol extends DefaultJsonProtocol {

  implicit val format1: JsonFormat[AddQualification] = jsonFormat13(AddQualification)
  implicit val formatq1: JsonFormat[EducationalQualification] = jsonFormat2(EducationalQualification)
  implicit val formatq2: JsonFormat[ProfessionalQualification] = jsonFormat5(ProfessionalQualification)
  implicit val formatq3: JsonFormat[OlResult] = jsonFormat10(OlResult)
  implicit val formatq4: JsonFormat[AlResult] = jsonFormat4(AlResult)
  implicit val format11: JsonFormat[CreateQualification] = jsonFormat10(CreateQualification)
  implicit val format2: JsonFormat[AddOlResult] = jsonFormat16(AddOlResult)
  implicit val format3: JsonFormat[AddAlResult] = jsonFormat10(AddAlResult)
  implicit val format44: JsonFormat[AddProfessionalQualification] = jsonFormat11(AddProfessionalQualification)
  implicit val format4: JsonFormat[AddEducationalQualification] = jsonFormat8(AddEducationalQualification)
  implicit val format5: JsonFormat[UpdateQualificationStatus] = jsonFormat7(UpdateQualificationStatus)
  implicit val format6: JsonFormat[GetQualification] = jsonFormat6(GetQualification)
  implicit val format61: JsonFormat[GetQualifications] = jsonFormat5(GetQualifications)
  implicit val format7: JsonFormat[SearchQualification] = jsonFormat12(SearchQualification)
  implicit  val format8 :JsonFormat[UpdateQualification] = jsonFormat10(UpdateQualification)
  implicit object QualificationMessageFormat extends RootJsonFormat[QualificationMessage] {
    def write(obj: QualificationMessage): JsValue =
      JsObject((obj match {
        case p: AddQualification => p.toJson
        case p: CreateQualification => p.toJson
        case p: AddEducationalQualification => p.toJson
        case p: AddProfessionalQualification => p.toJson
        case p: UpdateQualificationStatus => p.toJson
        case p: GetQualification => p.toJson
        case p: GetQualifications => p.toJson
        case p: SearchQualification => p.toJson
        case p: UpdateQualification =>p.toJson
        case unknown => deserializationError(s"json deserialize error: $unknown")
      }).asJsObject.fields)

    def read(json: JsValue): QualificationMessage =
      json.asJsObject.getFields("messageType") match {
        case Seq(JsString("addQualification")) => json.convertTo[AddQualification]
        case Seq(JsString("createQualification")) => json.convertTo[CreateQualification]
        case Seq(JsString("addProfessionalQualification")) => json.convertTo[AddProfessionalQualification]
        case Seq(JsString("addEducationalQualification")) => json.convertTo[AddEducationalQualification]
        case Seq(JsString("updateQualificationStatus")) => json.convertTo[UpdateQualificationStatus]
        case Seq(JsString("getQualification")) => json.convertTo[GetQualification]
        case Seq(JsString("getQualifications")) => json.convertTo[GetQualifications]
        case Seq(JsString("searchQualification")) => json.convertTo[SearchQualification]
        case Seq(JsString("updateQualification"))  =>json.convertTo[UpdateQualification]
        case unrecognized => serializationError(s"json serialization error $unrecognized")
      }
  }

}

//object M extends App {
//
//  import WatchMessageProtocol._
//
//  val i = Create("create", "eraga", "23121", "111", "tisslot", "black", "eranga")
//  val s = i.toJson.toString
//  println(s)
//  s.parseJson.convertTo[WatchMessage] match {
//    case i: Create => println(s"init $i")
//    case p: Put => println(s"put $p")
//    case g: Get => println(s"get $g")
//  }
//
//}


