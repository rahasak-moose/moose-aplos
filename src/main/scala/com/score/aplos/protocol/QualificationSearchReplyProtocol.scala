package com.score.aplos.protocol

import spray.json.{JsObject, JsValue, JsonFormat, RootJsonFormat, _}
import spray.json.{DefaultJsonProtocol, _}

case class QualificationMeta(offset: String, limit: String, count: String, total: String)

case class QualificationSearchReply(meta: QualificationMeta, projects: List[QualificationReply])

object QualificationSearchReplyProtocol extends DefaultJsonProtocol {
  implicit val metaFormat: JsonFormat[QualificationMeta] = jsonFormat4(QualificationMeta)
  import com.score.aplos.protocol.QualificationReplyProtocol._

  implicit object QualificationSearchReplyFormat extends RootJsonFormat[QualificationSearchReply] {
    override def write(obj: QualificationSearchReply): JsValue = {
      JsObject(
        ("meta", obj.meta.toJson),
        ("qualifications", obj.projects.toJson)
      )
    }

    override def read(json: JsValue) = ???
  }

}
