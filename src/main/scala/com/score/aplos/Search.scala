package com.score.aplos

import scala.annotation.tailrec

case class Node(id: String)

object Search extends App {
  var s = 0

  def findPasswordStrength(password: String): Int = {
    var s = 0
    //password.inits.flatMap(_.tails.toList.init).toList.toSet[String].foreach(s += _.length)
    password.inits.flatMap(_.tails.toSet.init).toSet[String].foreach(s += _.length)
    s
  }

  def flip(S: String): Int = {
    @tailrec
    def fOps(t: Int, f: Int, str: List[Char]): Int = {
      str match {
        case Nil =>
          Math.min(t, f)
        case h :: tail =>
          if (h == 'T') fOps(t + 1, f, tail) else fOps(t, f + 1, tail)
      }
    }

    fOps(0, 0, S.toList)
    //
    //    var t = 0
    //    var f = 0
    //    for (a <- S) {
    //      if (a == 'T')
    //        t += 1
    //      else
    //        f += 1
    //      f = Math.min(t, f)
    //    }
    //    f
  }

  //println(findPasswordStrength("test"))
  //println(Permutations.permutations("test"))
  case class CharCount(char: Char, count: Int)

  def compress(str: String) = {
    @tailrec
    def ops(chr: List[Char], counts: List[CharCount]): List[CharCount] = {
      chr match {
        case a :: b :: Nil =>
          if (a == b) {
            val cc = counts
              .find(_.char == a)
              .map(c => CharCount(a, c.count + 1))
              .getOrElse(CharCount(a, 1))
            val l = counts ::: List(cc)
            l
          } else {
            val cc1 = CharCount(a, 1)
            val cc2 = CharCount(b, 1)
            val l = counts ::: List(cc1, cc2)
            l
          }
        case a :: b :: tail =>
          if (a == b) {
            val x = counts.headOption.map(c => if (c.char == a) counts.dropRight(1) :+ CharCount(a, c.count + 2) else counts :+ CharCount(a, 2))
              .getOrElse(List(CharCount(a, 2)))
            ops(tail, x)
          } else {
            val cc1 = CharCount(a, 1)
            ops(List(b) ::: tail, counts ::: List(cc1))
          }
      }
    }

    ops(str.toList, List())
  }

  //println(compress("eeraaanga"))

  def compr(str: String) = {
    @tailrec
    def ops(chr: List[Char], counts: List[CharCount]): List[CharCount] = {
      chr match {
        case x :: tail =>
          val l = counts.lastOption.map(e => if (e.char == x) counts.dropRight(1) :+ CharCount(x, e.count + 1) else counts :+ CharCount(x, 1))
            .getOrElse(List(CharCount(x, 1)))
          ops(tail, l)
        case Nil =>
          counts
      }
    }

    ops(str.toList, List()).map(e => e.count + e.char.toString).mkString("")
  }

  def decom(str: String) = {
    @tailrec
    def ops(char: List[Char], str: String): String = {
      char match {
        case x :: y :: tail =>
          ops(tail, str + y.toString * x.toString.toInt)
        case Nil =>
          str
      }
    }

    ops(str.toList, "")
  }

  println(decom(compr("eeraanga")))

  def findMiss(l: List[Int]) = {
    def find(l: List[Int]): Option[Int] = {
      l match {
        case a :: b :: tail =>
          if (a + 1 == b) find(List(b) ::: tail)
          else Option(a + 1)
        case Nil =>
          None
      }
    }

    val x = l.sorted
    println(x)
    find(x)
  }

  println(findMiss(List(3, 2, 5, 6, 1, 8, 4)))

  def findSum(l: List[Int], s: Int) = {
    println(l.combinations(2).toList)
    println(l.combinations(2).toList.map(_.sum).contains(s))
  }

  findSum(List(1, 5, 3), 8)

}

object Permutations {
  def permutations(s: String): List[String] = {
    def merge(ins: String, c: Char): Seq[String] =
      for (i <- 0 to ins.length) yield
        ins.substring(0, i) + c + ins.substring(i, ins.length)

    if (s.length() == 1)
      List(s)
    else
      permutations(s.substring(0, s.length - 1)).flatMap { p =>
        merge(p, s.charAt(s.length - 1))
      }
  }
}
