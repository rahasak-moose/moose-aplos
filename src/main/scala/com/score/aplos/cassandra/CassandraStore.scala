package com.score.aplos.cassandra

import com.dataoperandz.cassper.Cassper
import com.score.aplos.util.CryptoFactory

import scala.concurrent.Await
import scala.concurrent.duration._

object CassandraStore extends TransStore with IdentityStore with BlobStore with QualificationStore with CassandraCluster {

  def init() = {
    // create keyspace
    session.execute(cassandraKeyspaceSchema)

    // migration via cassper
    // create udts, tables
    val builder = new Cassper().build("rahasak", session)
    builder.migrate("rahasak")

    // create admin user
    val admin = Identity(
      "admin", "admin", CryptoFactory.sha256("admin"), Set("admin"), Set(), None,
      Option("a1"), Option("a2"), Option("a2"), Option("admin"), None, None, None, None, None, Option("admin@vip.com"),
      None, None, None, None, None, None, None, None, None, None, None, None, "", 0,
      activated = true, disabled = false)
    CassandraStore.createIdentity(admin)
  }

  def createTrans(trans: Trans) = {
    Await.result(createTransAsync(trans), 10.seconds)
  }

  def isDoubleSpend(execer: String, actor: String, id: String): Boolean = {
    Await.result(searchTransAsync(execer, actor, id), 10.seconds).nonEmpty
  }

  def createIdentity(identity: Identity) = {
    Await.result(createIdentityAsync(identity), 10.seconds)
  }

  def updateIdentity(identity: Identity) = {
    Await.result(updateIdentityAsync(identity), 10.seconds)
  }

  def registerIdentity(identity: Identity) = {
    Await.result(registerIdentityAsync(identity), 10.seconds)
  }

  def updateDevice(did: String, owner: String, device: Device) = {
    Await.result(updateDeviceAsync(did, owner, device), 10.seconds)
  }

  def updateAnswer(did: String, owner: String, answer: Answer) = {
    Await.result(updateAnswerAsync(did, owner, answer), 10.seconds)
  }

  def updateRoles(did: String, owner: String, roles: Set[String]) = {
    Await.result(updateRolesAsync(did, owner, roles), 10.seconds)
  }

  def updatePassword(did: String, owner: String, password: String) = {
    Await.result(updatePasswordAsync(did, owner, password), 10.seconds)
  }

  def updateSalt(did: String, owner: String, salt: String) = {
    Await.result(updateSaltAsync(did, owner, salt), 10.seconds)
  }

  def updateAttempts(did: String, owner: String, attempts: Int) = {
    Await.result(updateAttemptsAsync(did, owner, attempts), 10.seconds)
  }

  def activateIdentity(did: String, owner: String, activated: Boolean) = {
    Await.result(activateIdentityAsync(did, owner, activated), 10.seconds)
  }

  def disableIdentity(did: String, owner: String, disabled: Boolean) = {
    Await.result(disableIdentityAsync(did, owner, disabled), 10.seconds)
  }

  def getIdentity(did: String, owner: String) = {
    Await.result(getIdentityAsync(did, owner), 10.seconds).headOption
  }

  def createBlob(blob: Blob) = {
    Await.result(createBlobAsync(blob), 10.seconds)
  }

  def updateBlob(blob: Blob) = {
    Await.result(updateBlobAsync(blob), 10.seconds)
  }

  def getBlob(id: String) = {
    Await.result(searchBlobAsync(id), 10.seconds).headOption
  }

  def createQualification(qualification: Qualification) = {
    Await.result(createQualificationAsync(qualification), 10.seconds)
  }

  def addOlResult(id: String, holder: String, olResult: OlResult) = {
    Await.result(addOlResultAsync(id, holder, olResult), 10.seconds)
  }

  def addAlResult(id: String, holder: String, alResult: AlResult) = {
    Await.result(addAlResultAsync(id, holder, alResult), 10.seconds)
  }

  def addEducationalQualification(id: String, holder: String, qualification: EducationalQualification) = {
    Await.result(addEducationalQualificationAsync(id, holder, qualification), 10.seconds)
  }

  def addProfessionalQualification(id: String, holder: String, qualification: ProfessionalQualification) = {
    Await.result(addProfessionalQualificationAsync(id, holder, qualification), 10.seconds)
  }

  def updateQualificationStatus(id: String, holder: String, status: Option[String]) = {
    Await.result(updateQualificationStatusAsync(id, holder, status), 10.seconds)
  }
  def updateQualification(qualification: Qualification):Unit ={
    Await.result(updateQualificationAsync(qualification), 10.seconds)
  }

  def getQualification(id: String, holder: String) = {
    Await.result(getQualificationAsync(id, holder), 10.seconds).headOption
  }

  def getQualifications(holder: String) = {
    Await.result(getQualificationsAsync(holder), 10.seconds)
  }

}
