package com.score.aplos.cassandra

import scala.concurrent.ExecutionContext.Implicits.global

import scala.concurrent.{Await, Future}


case class Slice(id: String, dataSpeed: Double, capacity: Double, quality: String, latency: Double, sla: String)

trait SliceStore extends CassandraCluster {
  import ctx._

  def createSliceAsync(slice: Slice): Future[Unit] = {
    val q = quote {
      query[Slice].insert(lift(slice))
    }
    ctx.run(q)
  }

  def updateBlobAsync(slice: Slice): Future[Unit] = {
    val q = quote {
      query[Slice]
        .filter(p => p.id == lift(slice.id))
        .update(
          _.capacity -> lift(slice.capacity),
          _.quality -> lift(slice.quality)
        )
    }
    ctx.run(q)
  }

  def searchSLiceAsync(id: String): Future[List[Slice]] = {
    val q = quote {
      query[Slice]
        .filter(p => p.id == lift(id))
        .take(1)
    }
    ctx.run(q)
  }

}
