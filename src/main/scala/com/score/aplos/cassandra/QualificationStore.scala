package com.score.aplos.cassandra

import java.util.Date

import com.datastax.driver.core.LocalDate

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Await, Future}

case class Contact(name: String,
                   email: String,
                   phone: String,
                   address: String)

case class OlResult(olMathematics: Option[String] = None,
                    olEnglish: Option[String] = None,
                    olSinhala: Option[String] = None,
                    olTamil: Option[String] = None,
                    olScience: Option[String] = None,
                    olHealthScience: Option[String] = None,
                    olReligion: Option[String] = None,
                    olArt: Option[String] = None,
                    olHistory: Option[String] = None,
                    olGeography: Option[String] = None)

case class AlResult(stream: Option[String],
                    subject1: Option[String],
                    subject2: Option[String],
                    subject3: Option[String])

case class EducationalQualification(hasDiploma: Option[Boolean] = None,
                                    nameDiploma: Option[String] = None)

case class ProfessionalQualification(hasExperience: Option[Boolean] = None,
                                     nameExperience: Option[String] = None,
                                     durationExperience: Option[String] = None,
                                     professionArea: Option[String] = None,
                                     professionName: Option[String] = None)

case class Qualification(id: String,
                        holder: String,

                        olMathematics: Option[String] = None,
                        olEnglish: Option[String] = None,
                        olSinhala: Option[String] = None,
                        olTamil: Option[String] = None,
                        olScience: Option[String] = None,
                        olHealthScience: Option[String] = None,
                        olReligion: Option[String] = None,
                        olArt: Option[String] = None,
                        olHistory: Option[String] = None,
                        olGeography: Option[String] = None,

                        alStream: Option[String] = None,
                        alSubject1: Option[String] = None,
                        alSubject2: Option[String] = None,
                        alSubject3: Option[String] = None,

                        hasDiploma: Option[Boolean] = None,
                        nameDiploma: Option[String] = None,

                        hasExperience: Option[Boolean] = None,
                        nameExperience: Option[String] = None,
                        durationExperience: Option[String] = None,

                        professionArea: Option[String] = None,
                        professionName: Option[String] = None,

                        status: Option[String] = None,

                        timestamp: Date = new Date())

trait QualificationStore extends CassandraCluster {

  import ctx._

  def createQualificationAsync(qualification: Qualification): Future[Unit] = {
    val q = quote {
      query[Qualification].insert(lift(qualification))
    }
    ctx.run(q)
  }

  def addOlResultAsync(id: String, holder: String, olResult: OlResult): Future[Unit] = {
    val q = quote {
      query[Qualification]
        .filter(r => r.id == lift(id) && r.holder == lift(holder))
        .update(
          _.olMathematics -> lift(olResult.olMathematics),
          _.olEnglish -> lift(olResult.olEnglish),
          _.olSinhala -> lift(olResult.olSinhala),
          _.olTamil -> lift(olResult.olTamil),
          _.olScience -> lift(olResult.olScience),
          _.olHealthScience -> lift(olResult.olHealthScience),
          _.olReligion -> lift(olResult.olReligion),
          _.olArt -> lift(olResult.olArt),
          _.olHistory -> lift(olResult.olHistory),
          _.olGeography -> lift(olResult.olGeography)
        )
    }
    ctx.run(q)
  }

  def addAlResultAsync(id: String, holder: String, alResult: AlResult): Future[Unit] = {
    val q = quote {
      query[Qualification]
        .filter(r => r.id == lift(id) && r.holder == lift(holder))
        .update(
          _.alStream -> lift(alResult.stream),
          _.alSubject1 -> lift(alResult.subject1),
          _.alSubject2 -> lift(alResult.subject2),
          _.alSubject3 -> lift(alResult.subject3)
        )
    }
    ctx.run(q)
  }

  def addEducationalQualificationAsync(id: String, holder: String, qualification: EducationalQualification): Future[Unit] = {
    val q = quote {
      query[Qualification]
        .filter(r => r.id == lift(id) && r.holder == lift(holder))
        .update(
          _.hasDiploma -> lift(qualification.hasDiploma),
          _.nameDiploma -> lift(qualification.nameDiploma)
        )
    }
    ctx.run(q)
  }

  def addProfessionalQualificationAsync(id: String, holder: String, qualification: ProfessionalQualification): Future[Unit] = {
    val q = quote {
      query[Qualification]
        .filter(r => r.id == lift(id) && r.holder == lift(holder))
        .update(
          _.hasExperience -> lift(qualification.hasExperience),
          _.nameExperience -> lift(qualification.nameExperience),
          _.durationExperience -> lift(qualification.durationExperience),
          _.professionArea -> lift(qualification.professionArea),
          _.professionName -> lift(qualification.professionName)
        )
    }
    ctx.run(q)
  }

  def updateQualificationStatusAsync(id: String, holder: String, status: Option[String]): Future[Unit] = {
    val q = quote {
      query[Qualification]
        .filter(r => r.id == lift(id) && r.holder == lift(holder))
        .update(
          _.status -> lift(status)
        )
    }
    ctx.run(q)
  }
  def updateQualificationAsync(qualification: Qualification) :Future[Unit]={
    val q = quote {
      query[Qualification]
        .filter(r => r.id == lift(qualification.id) && r.holder == lift(qualification.holder))
        .update(
      _.olMathematics->lift(qualification.olMathematics),
      _.olEnglish->lift(qualification.olEnglish),
      _.olSinhala->lift(qualification.olEnglish),
      _.olTamil->lift(qualification.olTamil),
      _.olScience->lift(qualification.olScience),
      _.olHealthScience->lift(qualification.olHealthScience),
      _.olReligion->lift(qualification.olReligion),
      _.olArt->lift(qualification.olArt),
      _.olHistory->lift(qualification.olHistory),
      _.olGeography->lift(qualification.olGeography),

      _.alStream->lift(qualification.alStream),
      _.alSubject1->lift(qualification.alSubject1),
      _.alSubject2->lift(qualification.alSubject2),
      _.alSubject3->lift(qualification.alSubject3),

      _.hasDiploma->lift(qualification.hasDiploma) ,
      _.nameDiploma->lift(qualification.nameDiploma),

      _.hasExperience->lift(qualification.hasExperience) ,
      _.nameExperience->lift(qualification.nameExperience),
      _.durationExperience->lift(qualification.durationExperience),

      _.professionArea->lift(qualification.professionArea),
      _.professionName->lift(qualification.professionName),

      _.status->lift(qualification.status),

      _.timestamp->lift(qualification.timestamp)
        )
    }
    ctx.run(q)

  }

  def getQualificationAsync(did: String, holder: String): Future[List[Qualification]] = {
    val q = quote {
      query[Qualification]
        .filter(p => p.holder == lift(holder))
        .take(1).allowFiltering
    }
    ctx.run(q)
  }

  def getQualificationsAsync(holder: String): Future[List[Qualification]] = {
    val q = quote {
      query[Qualification]
        .filter(p => p.holder == lift(holder))
        .allowFiltering
    }
    ctx.run(q)
  }

}

//object M5 extends App with QualificationStore {
//  // create document
//  import scala.concurrent.duration._
//  val hs = Await.result(getQualificationsAsync("eranga"), 10.seconds)
//  println(hs)
//}
