package com.score.aplos.cassandra

import java.util.Date

import com.dataoperandz.cassper.Cassper
import scala.concurrent.ExecutionContext.Implicits.global

import scala.concurrent.{Await, Future}
import scala.concurrent.duration._

case class Trans(id: String,
                 execer: String,
                 actor: String,
                 messageTyp: String,
                 message: String,
                 pubkey: String,
                 digsig: String,
                 timestamp: Date = new Date)

trait TransStore extends CassandraCluster {
  import ctx._

  def createTransAsync(trans: Trans): Future[Unit] = {
    val q = quote {
      query[Trans].insert(lift(trans))
    }
    ctx.run(q)
  }

  def searchTransAsync(execer: String, actor: String, id: String) = {
    val q = quote {
      query[Trans]
        .filter(p => p.execer == lift(execer) && p.actor == lift(actor) && p.id == lift(id))
        .take(1)
    }
    ctx.run(q)
  }
}

//object M3 extends App with TransStore with CassandraCluster {
//  println(Await.result(searchTransAsync("admin:admin", "AccountActor", "6121113"), 10.seconds))
//}
